<!-- popup box setup -->
<div class='popup_box_container'>
    <div class='popup_box'>
        <div id="head">

        </div>
        <div id="middle">

            <table>
                <tr class="borrowPopuptitleTr">
                    <td class="borrowPopuptitle">Boutique Name:</td>
                    <td id="borrowMovieName">
                        <div id="borrowMovieNameString">

                        </div>
                    </td>
                </tr>

                <tr id="tr_assetId">
                    <td class="borrowPopuptitle">Stock:</td>
                    <td id="borrowMovieAssetId">
                        <div id="borrowMovieAssetId">

                        </div>
                    </td>
                </tr>

                <tr id="tr_room">
                    <td class="borrowPopuptitle">Room:</td>
                    <td id="borrowMovieRoom"><input id="borrowMovieRoomInput" class='form-control' type='text' name='title' data-type='text' value='' autofocus=''></td>
                </tr>

            </table>

        </div>

        <div id="foot">
            <div class="actions">
                <div style='float:right; margin-left:20px'' class='submit round_btn btn bg-maroon' id="cancelBtn">Cancel</div>
                <div style='float:right' class='submit round_btn btn bg-olive' id="confirmBtn">Confirm</div>
            </div>
        </div>
    </div>
</div>

