<?php


echo "
<html lang=\"en\">
  <head>
    <title>Print Boutique Request</title>

    <link rel=\"shortcut icon\" href=\"images/favicon.ico\">
    <link rel=\"apple-touch-icon\" href=\"images/favicon.ico\">

    <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <link rel=\"stylesheet\" type=\"text/css\" href=\"css/main.css\" />

    <link rel=\"stylesheet\" type=\"text/css\" href=\"css/simplePagination.css\" />

    <link rel=\"stylesheet\" type=\"text/css\" href=\"3rdparty/timepicker/jquery.timepicker.min.css\" />
      <link rel=\"stylesheet\" type=\"text/css\" href=\"3rdparty/datetimepicker/jquery.datetimepicker.min.css\" />

    <!-- Theme style -->
    <link href=\"css/dist/css/AdminLTE.css\" rel=\"stylesheet\" type=\"text/css\" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the
    load. -->
    <link href=\"css/dist/css/skins/_all-skins.min.css\" rel=\"stylesheet\" type=\"text/css\" />
    <!--<link href=\"css/extra.css\" rel=\"stylesheet\" type=\"text/css\" />-->

    <!--image crop Library-->
    <link href=\"3rdparty/cropper/cropper.css\" rel=\"stylesheet\" type=\"text/css\" />

    <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>-->

    <link href=\"http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css\" rel=\"stylesheet\">

    <!-- Bootstrap 3.3.2 -->
    <link href=\"3rdparty/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />
    <!-- Font Awesome Icons -->
    <link href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />
    <!-- Ionicons -->
    <link href=\"http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css\" rel=\"stylesheet\" type=\"text/css\" />


<body><div class=\"container\"><h1>Boutique Request Order</h1>
    <div id=\"requestListViewContainer\">
        <div class=\"box\" id=\"list_boxer\">
            <div id=\"itemContainer\">

                <table style=\"height: 120px;\" width=\"100%\">
                    <tbody>
                    
                         <tr bgcolor=\"#f7edb4\">
                            <td id='status' style=\"width: 30%;font-weight: bold; font-size:18px; padding-left: 10px;\"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>
                    
                        <tr>
                            <td style=\"width: 20%;\">Room:</td>
                            <td id='room' style=\"width: 30%;\"></td>
                            <td style=\"width: 20%;\">Request Time:</td>
                            <td id='requesttime' style=\"width: 30%;\">&nbsp;</td>
                        </tr>
                        
                        <tr>
                            <td style=\"width: 20%;\">Boutique:</td>
                            <td id='title' style=\"width: 30%;\"></td>
                            <td style=\"width: 20%;\">Quantity:</td>
                            <td id='quantity".$order."' style=\"width: 30%;\"></td>
                        </tr>
                        
                        <tr>
                            <td style=\"width: 20%;\">Last Update Time:</td>
                            <td id='updatetime' style=\"width: 30%;\"></td>
                            <td style=\"width: 20%;\">Last Updated By:</td>
                            <td id='updatedby' style=\"width: 30%;\"></td>
                        </tr>
                        <tr>
                            <td style=\"width: 20%;\">Requested Delivery Time:</td>
                            <td id='deliverytime".$order."' style=\"width: 30%;\"></td>
                            <td style=\"width: 20%;\">Is Gift Wrap Required:</td>
                            <td id='isgiftwrapreqd".$order."' style=\"width: 30%;\"></td>
                        </tr>						
                    </tbody>
                </table>
                
                <div style='margin-top: 20px'> 
                    <span id='printBtn' class='request_item_blue_btn' style='margin-left: 20px;' onclick=\"myFunction()\">Print</span> 
                </div>
            </div>
        </div>
        
     
   
    </div>

<script>function myFunction() {window.print();}</script>
</body>
</html>"
?>