<div class='box detail_container'>
    <div class="box-header" style="padding-top:0px">
        <div style='width:100%;height:auto;float:left'>
            <h1 id='itemName'></h1>
            <select id='lang_selectionBox' name='language'>

                <option value='en'>English</option>
                <option value='zh_cn'>Simplied Chinese</option>
                <option value='fr'>French</option>
                <option value='jp'>Japanese</option>
                <option value='ar'>Arabic</option>
                <option value='es'>Spanish</option>
                <option value='de'>German</option>
                <option value='ko'>Korean</option>
                <option value='ru'>Russian</option>
                <option value='pt'>Portuguese</option>
                <option value='zh_hk'>Traditional Chinese</option>
                <option value='tr'>Turkish</option>

            </select>	
        </div>
    </div>

    <div class="box-body">
        <div class='half_width_container form-group' id='container_en'>
            <label for='title' id='titleLabel_en'>Title : </label>
            <input id='detailTitle_en' class='form-control' type='text' name='title' tabindex='1' data-type='text'
                   value='' autofocus=''>
        </div>
        <div class='half_width_container form-group' id='container_lang'>
            <label for='title' id='titleLabel_lang'>Title : </label>
            <input id='detailTitle_lang' class='form-control' type='text' name='title' tabindex='1' data-type='text'
                   value='' autofocus='' disabled>
        </div>

        <div class='half_width_container form-group' id='textEditor_en'>
            <label id='des_en'>
                Description :
            </label>
            <div id="textareaContainer">
                <input cols="80" id="editor1" name="editor1" rows="10">
                </input>
            </div>
        </div>
        <div class='half_width_container form-group' id='textEditor_lang'>
            <label id='des_lang'>
                Description :
            </label>
            <div id="textareaContainer">
            <textarea cols="80" id="editor2" name="editor2" rows="10">
            </textarea>
            </div>
        </div>

        <div class='one_third_width_container form-group' id='container_checkIn'>
            <div class='half_width_container_small' id='container_checkInDate'>
                <label for='title' id='priceLabel'>Price: </label>
                <input id='priceInput' type='text' name='title' value='' autofocus=''>
            </div>
        </div>

        <div class='one_third_width_container form-group' id='container_checkIn'>
            <div class='half_width_container_small' id='container_checkInDate'>
                <label for='rating' id='ratingLabel'>Rating(1.0-5.0) : </label>
                <div>
                    <input id='ratingInput' type='number' step='0.1' min='1' max='5' value='' autofocus=''>
                </div>
            </div>
        </div>

        <div class='one_third_width_container form-group'>
            <div class='full_width_container_small' id='container_checkInDate'>
                <label for='title' id='genreLabel'>Category : </label>
                <div style='width:auto'>
                    <div id="genre_list">
                    </div>
                </div>
            </div>

        </div>

        <hr>

        <hr>

        <div class='full_width_container form-group' id='textEditor_lang'>
            <div class='full_width_container_small' id='container_checkInDate'>
                <label for='title' id='posterLabel'>Poster 1:</label>

            </div>
            <div id="middle">
                <!--upload media UI-->
                <input id="takePictureField" type="file" accept="image/*">
                <div id="imageContainer">
                    <img style="width: 240px; height: 360px" id="uploadImage"/>
                </div>
            </div>
        </div>

        <div class='full_width_container form-group' id='textEditor_lang'>
            <div class='full_width_container_small' id='container_checkInDate'>
                <label for='title' id='posterLabel'>Poster 2:</label>

            </div>		
            <div id="middle">
                <!--upload media UI-->
                <input id="takePictureField1" type="file" accept="image/*">
                <div id="imageContainer1">
                    <img style="width: 240px; height: 360px" id="uploadImage1"/>
                </div>
            </div>
        </div>

        <div class='full_width_container form-group' id='textEditor_lang'>
            <div class='full_width_container_small' id='container_checkInDate'>
                <label for='title' id='posterLabel'>Poster 3:</label>

            </div>
            <div id="middle">
                <!--upload media UI-->
                <input id="takePictureField2" type="file" accept="image/*">
                <div id="imageContainer2">
                    <img style="width: 240px; height: 360px" id="uploadImage2"/>
                </div>
            </div>
        </div>

        <div class='full_width_container form-group' id='textEditor_lang'>
            <div class='full_width_container_small' id='container_checkInDate'>
                <label for='title' id='posterLabel'>Poster 4:</label>

            </div>		
            <div id="middle">
                <!--upload media UI-->
                <input id="takePictureField3" type="file" accept="image/*">
                <div id="imageContainer3">
                    <img style="width: 240px; height: 360px" id="uploadImage3"/>
                </div>
            </div>
        </div>
		
        <div class='save_cancel_container'>
            <div class='round_btn btn bg-maroon' id='delete_btn'>Delete Product</div>
            <div class='round_btn btn bg-olive' id='save_btn'>Save Product</div>
        </div>
    </div>
</div>