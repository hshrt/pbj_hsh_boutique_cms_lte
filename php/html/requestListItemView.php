<?php

$order = isset($_REQUEST['order']) ? $_REQUEST['order'] : '';

echo "<div id=\"requestListViewContainer\">
        <div class=\"box\" id=\"list_boxer\">
            <div id=\"itemContainer\">

                <table style=\"height: 120px;\" width=\"100%\">
                    <tbody>
                    
                         <tr bgcolor=\"#f7edb4\">
                            <td id='status".$order."' style=\"width: 30%;font-weight: bold; font-size:18px; padding-left: 10px;\"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>
                    
                        <tr>
                            <td style=\"width: 20%;\">Room:</td>
                            <td id='room".$order."' style=\"width: 30%;\"></td>
                            <td style=\"width: 20%;\">Request Time:</td>
                            <td id='requesttime".$order."' style=\"width: 30%;\">&nbsp;</td>
                        </tr>
                        
                        <tr>
                            <td style=\"width: 20%;\">Boutique:</td>
                            <td id='title".$order."' style=\"width: 30%;\"></td>
                            <td style=\"width: 20%;\">Quantity:</td>
                            <td id='quantity".$order."' style=\"width: 30%;\"></td>
                        </tr>
                        
                        <tr>
                            <td style=\"width: 20%;\">Last Update Time:</td>
                            <td id='updatetime".$order."' style=\"width: 30%;\"></td>
                            <td style=\"width: 20%;\">Last Updated By:</td>
                            <td id='updatedby".$order."' style=\"width: 30%;\"></td>
                        </tr>
                        <tr>
                            <td style=\"width: 20%;\">Requested Delivery Time:</td>
                            <td id='deliverytime".$order."' style=\"width: 30%;\"></td>
                            <td style=\"width: 20%;\">Is Gift Wrap Required:</td>
                            <td id='isgiftwrapreqd".$order."' style=\"width: 30%;\"></td>							
                        </tr>
                    </tbody>
                </table>
                
                <div style='margin-top: 20px'> 
                    <span id='processBtn".$order."' order='".$order."' >Process</span> 
                    <span id='printBtn".$order."' order='".$order."' class='request_item_blue_btn' style='margin-left: 20px;'>Print</span> 
                    <span id='cancelBtn".$order."' order='".$order."' class='request_item_red_btn_on'>Cancel</span>
                </div>
            </div>
            
            <div id=\"noResultMsg\">There are no Request List</div>
       
        </div>
        
        <div class=\"box-footer\">

        </div>
        
    </div>"


?>