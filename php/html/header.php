<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo htmlspecialchars( HEADER_TEXT )?></title>

    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="apple-touch-icon" href="images/favicon.ico">

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <link rel="stylesheet" type="text/css" href="css/main.css" />

    <link rel="stylesheet" type="text/css" href="css/simplePagination.css" />

    <link rel="stylesheet" type="text/css" href="3rdparty/timepicker/jquery.timepicker.min.css" />
      <link rel="stylesheet" type="text/css" href="3rdparty/datetimepicker/jquery.datetimepicker.min.css" />

    <!-- Theme style -->
    <link href="css/dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the
    load. -->
    <link href="css/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <!--<link href="css/extra.css" rel="stylesheet" type="text/css" />-->

    <!--image crop Library-->
    <link href="3rdparty/cropper/cropper.css" rel="stylesheet" type="text/css" />

    <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>-->

    <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

    <!-- Bootstrap 3.3.2 -->
    <link href="3rdparty/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font- awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />



    <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
    <script src="3rdparty/jquery-2.1.1.min.js"></script>

      <script src="3rdparty/datetimepicker/jquery.datetimepicker.min.js"></script>

   <!-- <script src="http://libs.baidu.com/jquery/1.9.0/jquery.js"></script>-->
    <!--<script src="3rdparty/jquery_2.2.3.js"></script>-->

    <script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.3.3/underscore-min.js" type="text/javascript"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/backbone.js/0.9.2/backbone-min.js" type="text/javascript"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/backbone-localstorage.js/1.0/backbone.localStorage-min.js"></script>
    <script src="js/jquery.address/jquery.address-1.6.min.js"></script>
    <script src="3rdparty/dotdotdot/jquery.dotdotdot.min.js"></script>
    <script src="3rdparty/jquery-ui.min.js"></script>
    <script src="3rdparty/jquery.rowsorter.min.js"></script>
    <script src="3rdparty/timepicker/jquery.timepicker.js"></script>
    <script src="3rdparty/combodate/combodate.js"></script>
    <script src="3rdparty/combodate/moment.js"></script>
    
    <script src="3rdparty/spin/spin.js"></script>
    <script src="3rdparty/spin/jquery.spin.js"></script>

    <script type="text/javascript" src="3rdparty/pagination/jquery.simplePagination.js"></script>

    <script src="3rdparty/ckeditor/ckeditor.js?<?php echo time(); ?>"></script>
    <script src="3rdparty/ckeditor/adapters/jquery.js?<?php echo time(); ?>"></script>

    <script src="3rdparty/cropper/cropper.js"></script>

    <!--<script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5.js"></script>-->
    <script src="3rdparty/phpjs/md5.js"></script>
    <script src="3rdparty/phpjs/utf8_encode.js"></script>

    <script src="js/app.js?<?php echo time(); ?>"></script>
    <script src="js/controller/general_ui/yesnopopup.js?<?php echo time(); ?>"></script>
    <script src="js/controller/signInView.js?<?php echo time(); ?>"></script>
    <script src="js/controller/subTopBar.js?<?php echo time(); ?>"></script>
    <script src="js/controller/boutique/genreList.js?<?php echo time(); ?>"></script>
    <script src="js/controller/boutique/latestList.js?<?php echo time(); ?>"></script>
    <script src="js/controller/boutique/languageList.js?<?php echo time(); ?>"></script>
    <script src="js/controller/boutique/subtitleList.js?<?php echo time(); ?>"></script>
    <script src="js/controller/boutique/configList.js?<?php echo time(); ?>"></script>
	<script src="js/controller/boutique/deleteList.js?<?php echo time(); ?>"></script>
    <script src="js/controller/boutique/boutiqueList.js?<?php echo time(); ?>"></script>
	<script src="js/controller/boutique/pendingList.js?<?php echo time(); ?>"></script>
    <script src="js/controller/boutique/requestList.js?<?php echo time(); ?>"></script>
    <script src="js/controller/boutique/divisionList.js?<?php echo time(); ?>"></script>
    <script src="js/controller/boutique/addGenrePopup.js?<?php echo time(); ?>"></script>
    <script src="js/controller/boutique/addLanguagePopup.js?<?php echo time(); ?>"></script>
    <script src="js/controller/boutique/addSubtitlePopup.js?<?php echo time(); ?>"></script>
    <script src="js/controller/boutique/addBorrowPopup.js?<?php echo time(); ?>"></script>
    <script src="js/controller/boutique/addInventoryPopup.js?<?php echo time(); ?>"></script>
    <script src="js/controller/boutique/addLatestPopup.js?<?php echo time(); ?>"></script>
    <script src="js/controller/boutique/addDivisionPopup.js?<?php echo time(); ?>"></script>
    <script src="js/controller/boutique/movieDetail.js?<?php echo time(); ?>"></script>
	<script src="js/controller/boutique/boutiqueDetail.js?<?php echo time(); ?>"></script>
	<script src="js/controller/boutique/addApprovalPopup.js?<?php echo time(); ?>"></script>
	<script src="js/controller/boutique/openingHours.js?<?php echo time(); ?>"></script>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
  </head>

