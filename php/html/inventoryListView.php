<?php



echo "<div id=\"inventoryListViewContainer\">
    <div class=\"box\" id=\"list_boxer\">
        <div class=\"box-header\" style=\"padding-top:0px\">
            <h3 style=\"float:left\" id=\"list_head_title\">Movies</h3>
            <div class='submit round_btn btn bg-olive' id='addBtn'>+Add Movie</div>

            <input id=\"search\" class=\"form-control\" type='text' name='title' tabindex='1' data-type='text' placeholder=\"Search...\" autofocus=''>
        </div>

        <div class=\"box-body\">
        
             <div id=\"headerContainer\">
                <div id=\"name\" class=\"tableHead\">Movie Title</div>
                <div id=\"year\" class=\"tableHead\">Year</div>
                <div id=\"genre\" class=\"tableHead\">Genre</div>
                <div id=\"stock\" class=\"tableHead\">Available Stock</div>
                <div id=\"borrow\" class=\"tableHead\"></div>
            </div>

            <div id=\"itemContainer\">

            </div>
            <div id=\"noResultMsg\">There are no Moive</div>
            <div id='paginationContainer'>

            </div>
        </div>

        <div class=\"box-footer\">

        </div>
    </div>
</div>"


?>