<!-- popup box setup -->
<div class='popup_box_container'>
    <div class='popup_box addKey_box'>
        <div id="head">
            <h1 id="create_box_title">Translation</h1>
            <div class='closeBtn' id='closeBtn'></div>
        </div>

        <div id="middle">
            <h4>English</h4>
            <input id="English" class='form-control' type='text' name='title' data-type='text' value='' autofocus=''>

            <h4>Simplified Chinese</h4>
            <input id="SimplifiedChinese" class='form-control' type='text' name='title' data-type='text' value='' autofocus=''>

            <h4>French</h4>
            <input id="French" class='form-control' type='text' name='title' data-type='text' value='' autofocus=''>

            <h4>Japanese</h4>
            <input id="Japanese" class='form-control' type='text' name='title' data-type='text' value='' autofocus=''>

            <h4>Arabic</h4>
            <input id="Arabic" class='form-control' type='text' name='title' data-type='text' value='' autofocus=''>

            <h4>Spanish</h4>
            <input id="Spanish" class='form-control' type='text' name='title' data-type='text' value='' autofocus=''>

            <h4>German</h4>
            <input id="German" class='form-control' type='text' name='title' data-type='text' value='' autofocus=''>

            <h4>Korean</h4>
            <input id="Korean" class='form-control' type='text' name='title' data-type='text' value='' autofocus=''>
            <h4>Russian</h4>
            <input id="Russian" class='form-control' type='text' name='title' data-type='text' value='' autofocus=''>
            <h4>Portuguese</h4>
            <input id="Portuguese" class='form-control' type='text' name='title' data-type='text' value='' autofocus=''>
            <h4>Traditional Chinese</h4>
            <input id="TraditionalChinese" class='form-control' type='text' name='title' data-type='text' value='' autofocus=''>
            <h4>Turkish</h4>
            <input id="Turkish" class='form-control' type='text' name='title' data-type='text' value='' autofocus=''>

        </div>

        <div id="foot">
            <div class="actions">
                <div style='float:left' class='submit round_btn  btn bg-maroon' id="deleteBtn">Delete Subtitle</div>
                <div style='float:right' class='submit round_btn btn bg-olive' id="saveCreateBtn">Save</div>
            </div>
        </div>
    </div>
</div>