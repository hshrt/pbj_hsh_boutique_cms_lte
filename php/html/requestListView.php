<?php

echo "<div id=\"inventoryListViewContainer\">
    <div class=\"box\" id=\"list_boxer\">
        <div class=\"box-header\" style=\"padding-top:0px\">
          
            <h3 style=\"float:left;width:100%;height:100px;\" id=\"list_head_title\" >Request List</h3>
       
            <div style='display: inline-block;width:100%;'>
                <div style='width:25%;float:left'>Date  </div>
                <div style='width:25%;float:left'>Status</div>
                <div style='width:25%;float:left'>Room</div>
              
            </div>
            
            <div style='display: inline-block;width:100%;'>
               <input style='width:20%;height:20px;margin-right:5%;' id='dateInput' type='text' name='date'>
               <select style='width:20%;height:20px;margin-right:5%;' id='statusSelectionbox' name='status'>
                    <option value='-1' selected='selected'>All</option>
                    <option hidden value='0'>Available</option>
                    <option value='1'>Waiting for shipment</option>
					<option value='2'>Waiting for delivery</option>
                    <option value='3'>Delivered</option>
                    <option value='4'>Waiting for return</option>
                    <option value='5'>Returned</option>
                    <option value='6'>Cancelled</option>
                    <option hidden value='7'>Booked</option>
                </select>
               
                <input style='width:20%;height:20px;margin-right:5%;' id='roomInput' type='text' name='room'>
                 <div style='height:30px' class='search_round_btn btn bg-olive' id='searchBtn'>Search</div>
                
              
            </div>
     </div>

        <div class=\"box-body\">
        
             <div id=\"headerContainer\">
             
            </div>

            <div id=\"itemContainer\">

            </div>
            <div id=\"noResultMsg\">There are no product</div>
            <div id='paginationContainer'>

            </div>
        </div>

        <div class=\"box-footer\">

        </div>
    </div>
</div>"


?>