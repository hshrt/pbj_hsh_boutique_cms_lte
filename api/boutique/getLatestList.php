<?php
require("../../config.php");

ini_set("display_errors", true);

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

//using itemId is for get one specific item
//using parentId is for get a list of items which is child of that parent

$page = 0;
$itemPerPage = 15;
$getCount = null;
$genre = null;
$lang = "en";

if (isset($_REQUEST['lang'])) {
    $lang = $_REQUEST['lang'];
} else {
    $lang = 'en';
}


$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

$sql = "SELECT DISTINCT boutique_latest.id As id, boutique_latest.productId As movieId, m1.poster As poster, m1.boutiqueTitleEng As boutiqueTitleEng, m1.movieTitle As movieTitle            
        FROM boutique_latest
        LEFT JOIN (SELECT boutique.id As movieId, boutique.posterurl As poster, title.boutiqueTitleEng As boutiqueTitleEng, title.movieTitle As movieTitle
                  FROM boutique 
                  INNER JOIN 
                        (SELECT boutique.titleId AS titleId, 
                        (CASE boutique_dictionary." . $lang . " WHEN '' THEN boutique_dictionary.en ELSE boutique_dictionary." . $lang . " END ) AS movieTitle,
                         boutique_dictionary.en AS boutiqueTitleEng
                         FROM boutique 
                         INNER JOIN boutique_dictionary
                         ON boutique.titleId = boutique_dictionary.id) title
                  ON title.titleId = boutique.titleId 
                  WHERE boutique.isVoid = 0 ) m1
        ON boutique_latest.productId = m1.movieId
        ORDER BY boutique_latest.ordering ASC ";

$st = $conn->prepare($sql);
$st->execute();
$list = array();

while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
    $list[] = $row;
}

$conn = null;

if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'get latest good', $list);
} else {
    echo returnStatus(0, 'get latest fail');
}


?>
