<?php
require("../../config.php");

ini_set("display_errors", true);

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

//using itemId is for get one specific item
//using parentId is for get a list of items which is child of that parent

$page = 0;
$itemPerPage = 15;
$getCount = null;
$movieId = null;
$genre = null;
$lang = "en";
$divisionId = null;

if (isset($_REQUEST['getCount'])) {
    $getCount = $_REQUEST['getCount'];
}

if (isset($_REQUEST['movieId'])) {
    $movieId = $_REQUEST['movieId'];
}

if (isset($_REQUEST['lang'])) {
    $lang = $_REQUEST['lang'];
}

if (isset($_REQUEST['divisionId'])) {
    $divisionId = $_REQUEST['divisionId'];
}

$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");


if (!empty($getCount)) {
    $sql = "SELECT count(*) AS totalNum FROM boutique ";
    $sql = $sql."WHERE isVoid = 0 ";

    if (isset($_REQUEST['movieId'])) {
        $sql = $sql." AND boutique.id = ".$movieId."' ";
    }
    $st = $conn->prepare($sql);
    $st->execute();
    $list = array();

    while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
        $list[] = $row;
    }

    if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
        echo returnStatus(1, 'get boutique count good', $list);
    } else {
        echo returnStatus(0, 'get boutique count fail');
    }

} else {

    $sql = "SELECT DISTINCT boutique.id As movieId, boutique.titleId AS titleId, boutique.descriptionId As descriptionId, boutique.isAuth as auth, title.movieTitle AS movieTitle, 
                            description.movieDescription AS movieDescription, boutique.price AS price, boutique.remark AS remark,
                            boutique.divisionId As divisionId, boutique.rating As rating, 
                            (CASE WHEN m1.available IS NULL THEN 0 ELSE m1.available END) AS available, 
                            boutique.posterurl As poster, boutique.posterurl1 As poster1, boutique.posterurl2 As poster2, boutique.posterurl3 As poster3, (CASE WHEN m2.stock IS NULL THEN 0 ELSE m2.stock END) As stock
            FROM boutique
            LEFT JOIN (SELECT COUNT(boutique_inventory.id) AS available,
                        boutique_inventory.productId As movieId
                      FROM boutique_inventory 
                      WHERE available = 1 AND isVoid = 0 
                      GROUP BY  boutique_inventory.productId) m1
            ON boutique.id = m1.movieId
            
             LEFT JOIN (SELECT COUNT(boutique_inventory.id) AS stock,
                        boutique_inventory.productId As movieId
                      FROM boutique_inventory 
                      WHERE isVoid = 0 
                      GROUP BY  boutique_inventory.productId) m2
            ON boutique.id = m2.movieId
            
            INNER JOIN 
                (SELECT boutique.titleId AS titleId,
                 (CASE boutique_dictionary." . $lang . " WHEN '' THEN boutique_dictionary.en ELSE boutique_dictionary." . $lang . " END ) AS movieTitle
                 FROM boutique 
                 INNER JOIN boutique_dictionary
                 ON boutique.titleId = boutique_dictionary.id) title
            ON title.titleId = boutique.titleId 
            
            INNER JOIN 
                (SELECT boutique.descriptionId AS descriptionId,
                (CASE boutique_dictionary." . $lang . " WHEN '' THEN boutique_dictionary.en ELSE boutique_dictionary." . $lang . " END ) AS movieDescription
                 FROM boutique 
                 INNER JOIN boutique_dictionary
                 ON boutique.descriptionId = boutique_dictionary.id) description
            ON description.descriptionId = boutique.descriptionId ";

    $sql = $sql."WHERE isVoid = 0 ";

    if (isset($_REQUEST['movieId'])) {
        $sql = $sql." AND boutique.id = '".$movieId."' ";
    }
    $sql = $sql."GROUP BY title.movieTitle ORDER BY title.movieTitle ASC;";


    $st = $conn->prepare($sql);
    $st->execute();
    $list = array();

    while ($row = $st->fetch(PDO::FETCH_ASSOC)) {

        $currentMovieId = $row["movieId"];

        $genreString = null;
        $genreId = null;
        $sql = "SELECT DISTINCT title.genreTitle As genreTitle, boutiques_category.categoryId As genreId 
                  FROM boutiques_category
            
                INNER JOIN
                    (SELECT boutique_category.id AS id, 
                    (CASE boutique_dictionary." . $lang . " WHEN '' THEN boutique_dictionary.en ELSE boutique_dictionary." . $lang . " END ) AS genreTitle
                      FROM boutique_category
                      INNER JOIN boutique_dictionary
                      ON boutique_category.titleId = boutique_dictionary.id) title
                ON title.id = boutiques_category.categoryId
                WHERE boutiques_category.productId = '".$currentMovieId."' ";

        $st2 = $conn->prepare($sql);
        $st2->execute();

        while ($row2 = $st2->fetch(PDO::FETCH_ASSOC)) {

            if($genreString == null){
                $genreString = $row2["genreTitle"];
            } else {
                $genreString = $genreString.", ".$row2["genreTitle"];
            }

            if($genreId == null){
                $genreId = $row2["genreId"];
            } else {
                $genreId = $genreId.",".$row2["genreId"];
            }

        }
        $row["genre"] = $genreString;
        $row["genreId"] = $genreId;

        $currentDivisionId = $row["divisionId"];
        $divisionString = null;
        $divisionId = null;

        $sql = "SELECT DISTINCT title.genreTitle As genreTitle, boutique_division.id As genreId 
                  FROM boutique_division
            
                INNER JOIN
                    (SELECT boutique_division.id AS id, 
                    (CASE boutique_dictionary." . $lang . " WHEN '' THEN boutique_dictionary.en ELSE boutique_dictionary." . $lang . " END ) AS genreTitle
                      FROM boutique_division
                      INNER JOIN boutique_dictionary
                      ON boutique_division.titleId = boutique_dictionary.id) title
                ON title.id = boutique_division.id
                WHERE boutique_division.id = '".$currentDivisionId."' ";

        $st2 = $conn->prepare($sql);
        $st2->execute();

        while ($row2 = $st2->fetch(PDO::FETCH_ASSOC)) {

            if($divisionString == null){
                $divisionString = $row2["genreTitle"];
            } else {
                $divisionString = $divisionString.", ".$row2["genreTitle"];
            }

        }
        $row["division"] = $divisionString;

        $list[] = $row;
    }

    $conn = null;

    if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
        echo returnStatus(1, 'get boutique good', $list);
    } else {
        echo returnStatus(0, 'get boutique fail');
    }
}

?>
