<?php
require( "../../config.php" );

ini_set( "display_errors", true );

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

//using itemId is for get one specific item
//using parentId is for get a list of items which is child of that parent

$type = '';
$id = '';

if(isset($_REQUEST['type'])){
    $type = $_REQUEST['type'];
}

if(isset($_REQUEST['id'])){
    $id = $_REQUEST['id'];
}

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

if($type == "genre"){
    $sql = "select  count(*) as totalNum from boutiques_category where boutiques_category.categoryId ='".$id."' ";
} else if($type == "division"){
    $sql = "select  count(*) as totalNum from boutique where boutique.divisionId ='".$id."' AND boutique.isVoid = 0";
} else if($type == "movie"){
    $sql = "select  count(*) as totalNum from boutique_inventory where boutique_inventory.productId ='".$id."' AND boutique_inventory.isVoid = 0";
}

$st = $conn->prepare($sql);

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

$conn = null;

if($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'get count good', $list);
}
else{
    echo returnStatus(0, 'get count fail');
}

?>
