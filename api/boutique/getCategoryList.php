<?php
require( "../../config.php" );

ini_set( "display_errors", true );

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

//using itemId is for get one specific item
//using parentId is for get a list of items which is child of that parent

$page = 0;
$itemPerPage = 15;
$getCount = null;

if(isset($_REQUEST['getCount'])){
    $getCount = $_REQUEST['getCount'];
}

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

if(!empty($getCount)){
    $sql = "select  count(*) as totalNum from boutique_category where boutique_category.titleId !='' ";
} else {
    $sql = "SELECT  boutique_category.id As id,
                boutique_category.titleId As titleId,
                boutique_dictionary.en AS en,
                (CASE boutique_dictionary.zh_cn WHEN '' THEN boutique_dictionary.en ELSE boutique_dictionary.zh_cn END ) AS zh_cn,
                (CASE boutique_dictionary.fr WHEN '' THEN boutique_dictionary.en ELSE boutique_dictionary.fr END ) AS fr,
                (CASE boutique_dictionary.jp WHEN '' THEN boutique_dictionary.en ELSE boutique_dictionary.jp END ) AS jp,
                (CASE boutique_dictionary.ar WHEN '' THEN boutique_dictionary.en ELSE boutique_dictionary.ar END ) AS ar,
                (CASE boutique_dictionary.es WHEN '' THEN boutique_dictionary.en ELSE boutique_dictionary.es END ) AS es,
                (CASE boutique_dictionary.de WHEN '' THEN boutique_dictionary.en ELSE boutique_dictionary.de END ) AS de,
                (CASE boutique_dictionary.ko WHEN '' THEN boutique_dictionary.en ELSE boutique_dictionary.ko END ) AS ko,
                (CASE boutique_dictionary.ru WHEN '' THEN boutique_dictionary.en ELSE boutique_dictionary.ru END ) AS ru,
                (CASE boutique_dictionary.pt WHEN '' THEN boutique_dictionary.en ELSE boutique_dictionary.pt END ) AS pt,
                (CASE boutique_dictionary.zh_hk WHEN '' THEN boutique_dictionary.en ELSE boutique_dictionary.zh_hk END ) AS zh_hk,
                (CASE boutique_dictionary.tr WHEN '' THEN boutique_dictionary.en ELSE boutique_dictionary.tr END ) AS tr
            FROM boutique_category 
            INNER JOIN boutique_dictionary
            ON boutique_category.titleId = boutique_dictionary.id
            ORDER BY boutique_category.ordering ASC";
}


$st = $conn->prepare($sql);

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

$conn = null;

if($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'get itemList good', $list);
}
else{
    echo returnStatus(0, 'get itemList fail');
}

?>
