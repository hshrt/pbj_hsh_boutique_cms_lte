<?php 

ini_set( "display_errors", true );
require( "../../config.php" );

require("../../php/inc.appvars.php");

if(isset($_REQUEST['id'])){
    $id= $_REQUEST["id"];
}

// Insert the Article
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "SELECT id, en, zh_hk, zh_cn, jp, fr, ar, es, de, ko, ru, pt, tr from boutique_dictionary where id = :id";

$st = $conn->prepare ($sql);
$st->bindValue( ":id", $id, PDO::PARAM_STR);
$st->execute();

$list = array();
while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}

$conn = null;
echo returnStatus(1 , 'get Language Map good',$list);

?>
