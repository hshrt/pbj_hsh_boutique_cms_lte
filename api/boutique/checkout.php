<?php

ini_set("display_errors", true);

require("../../config.php");
require("../../php/func_nx.php");
require("../../php/inc.appvars.php");

session_start();

$room = null;
$lastUpdateBy = null ;

if (isset($_POST["room"]) && $_POST["room"] != null && strlen($_POST["room"]) > 1) {
    $room = $_POST["room"];
}

$lastUpdateBy = "system";

//setup DB
$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

$sql = "UPDATE boutique_order_history SET enable = 0, lastUpdate=now(),lastUpdateBy=:lastUpdateBy WHERE roomId = :room;";

//echo $sql;

$st = $conn->prepare($sql);

$st->bindValue(":room", $room, PDO::PARAM_STR);
$st->bindValue(":lastUpdateBy", $lastUpdateBy, PDO::PARAM_STR);

$st->execute();

if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'checkout good');
} else {
    echo returnStatus(0, 'checkout fail');
}


$conn = null;


?>
