<?php

ini_set("display_errors", true);
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();

$movieId = isset($_POST['movieId']) ? $_POST['movieId'] : '';
$roomId = isset($_POST['roomId']) ? $_POST['roomId'] : '';
$inventoryId = isset($_POST['inventoryId']) ? $_POST['inventoryId'] : '';
$statusId = isset($_POST['statusId']) ? $_POST['statusId'] : '1';
$requestId = isset($_POST['requestId']) ? $_POST['requestId'] : '';
$deliveryTime = isset($_POST['deliveryTime']) ? $_POST['deliveryTime'] : '';
$quantity = isset($_POST['quantity']) ? $_POST['quantity'] : '';
$isGiftWrapRequired = isset($_POST['isGiftWrap']) ? $_POST['isGiftWrap'] : '';
$session = ($_SESSION == null) ? "bedside" : $_SESSION['email'];
$inventId = '';
$stckId = '';


$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

$tempqty = $quantity;
$reqId = '';

$sql = "INSERT INTO boutique_order_history (roomId, requestTime, productId, reqDeliveryTime, inventoryId, enable, statusId, lastUpdate, lastUpdateBy, quantity, isGiftWrap) 
    VALUES (:roomId, now(), :productId, :reqDeliveryTime, :inventoryId, 1, :statusId, now(), :lastUpdateBy, :quantity, :isGiftWrap)";
$st = $conn->prepare($sql);
$st->bindValue(":roomId", $roomId, PDO::PARAM_STR);
$st->bindValue(":productId", $movieId, PDO::PARAM_STR);
$st->bindValue(":inventoryId", $inventoryId, PDO::PARAM_STR);
$st->bindValue(":statusId", $statusId, PDO::PARAM_STR);
$st->bindValue(":reqDeliveryTime", $deliveryTime, PDO::PARAM_STR);
$st->bindValue(":lastUpdateBy", $session, PDO::PARAM_STR);
$st->bindValue(":quantity", $quantity, PDO::PARAM_STR);
$st->bindValue(":isGiftWrap", $isGiftWrapRequired, PDO::PARAM_STR);
$st->execute();

	
$sql = "SELECT boutique_inventory.id As id, boutique_inventory.stockId As assetId
            FROM boutique_inventory
            WHERE boutique_inventory.isVoid = 0 
                  AND boutique_inventory.available = 1 
                  AND boutique_inventory.productId = '" . $movieId . "' ORDER BY boutique_inventory.stockId ASC";

$st1 = $conn->prepare($sql);
$st1->execute();

$sql = "SELECT LAST_INSERT_ID() as totalNum";
$st = $conn->prepare($sql);
$st->execute();

while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
	$reqId = $row["totalNum"];
}

while (($tempqty > 0) && $row = $st1->fetch(PDO::FETCH_ASSOC)) {
	$tempqty = $tempqty - 1;
    $inventId = $row["id"];
	$stckId = $row["assetId"];
	$sql = "UPDATE boutique_order_history SET inventoryId=:inventoryId, statusId=1, lastUpdate=now(), lastUpdateBy=:lastUpdateBy WHERE id='".$reqId."' ";
	$st = $conn->prepare($sql);
	$st->bindValue(":inventoryId", $inventId, PDO::PARAM_STR);
	$st->bindValue(":lastUpdateBy", $session, PDO::PARAM_STR);
    $st->execute();
/*if($passed){
echo returnStatus(1, $reqId+'get default requestList pass');
} else {
echo returnStatus(0, $st->errorInfo(),$reqId+'get default requestList not pass');
}*/
	$sql = "UPDATE boutique_inventory SET available=0, lastUpdate=now(), reqId=:reqId, lastUpdateBy=:lastUpdateBy WHERE id ='" . $inventId . "' AND stockId ='" . $stckId . "'";
    $st = $conn->prepare($sql);
	$st->bindValue(":reqId", $reqId, PDO::PARAM_STR);
    $st->bindValue(":lastUpdateBy", $session, PDO::PARAM_STR);
    $st->execute();
	//break;
}

if ($st->rowCount() > 0) {

    if(strlen($inventId) > 0) {


        if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
            echo returnStatus(1, 'add borrow record good');
        } else {
            echo returnStatus(0, 'add borrow record fail');
        }
    } else {
        echo returnStatus(1, 'add borrow record good');
    }
} else {
    echo returnStatus(0, 'Add borrow record fail');
}

$conn = null;
return 0;

?>