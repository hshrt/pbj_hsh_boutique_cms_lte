<?php
require("../../config.php");

ini_set("display_errors", true);

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();

$date = null;
$status = null;
$room = null;
$lang = "en";
$sqlForFilter = '';

if (isset($_REQUEST["date"]) && $_REQUEST["date"] != null && strlen($_REQUEST["date"]) > 1) {
    $date = $_REQUEST["date"];
    $sqlForFilter = $sqlForFilter . " AND DATE(hist.requestTime) ='" . $date . "' ";
}

if (isset($_REQUEST["status"]) && $_REQUEST["status"] != null && strlen($_REQUEST["status"]) > 0 && $_REQUEST["status"] > 0) {
    $status = $_REQUEST["status"];
    $sqlForFilter = $sqlForFilter . " AND hist.statusId = " . $status . " ";
}

if (isset($_REQUEST["room"]) && $_REQUEST["room"] != null && strlen($_REQUEST["room"]) > 1) {
    $room = $_REQUEST["room"];
    $sqlForFilter .= $sqlForFilter . " AND hist.roomId = '" . $room . "' ";
}

if (isset($_REQUEST["lang"])) {
    $lang = $_REQUEST["lang"];
}

//using itemId is for get one specific item
//using parentId is for get a list of items which is child of that parent

$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

$sql = "SELECT 
                hist.id As id,
                hist.roomId As room,
                inventory.assetId As assetId,
                moviedetail.productId As productId,
                moviedetail.movieTitle As title,
                hist.statusId As statusId,
                hist.requestTime As requesttime, 
                hist.lastUpdate As lastupdate, 
				hist.reqDeliveryTime As reqDeliveryTime, 
				hist.quantity As quantity, 
				hist.isGiftWrap As isGiftWrap, 
                hist.lastUpdateBy As lastupdateby
            FROM boutique_order_history hist 
            
            LEFT JOIN 
                (SELECT boutique.id As productId, 
                (CASE boutique_dictionary." . $lang . " WHEN '' THEN boutique_dictionary.en ELSE boutique_dictionary." . $lang . " END ) AS movieTitle
                 FROM boutique 
                 INNER JOIN boutique_dictionary
                 ON boutique.titleId = boutique_dictionary.id) moviedetail
            ON moviedetail.productId = hist.productId
            
            LEFT JOIN (SELECT boutique_inventory.id As inventoryId, boutique_inventory.stockId As assetId 
                        FROM boutique_inventory) inventory
            ON inventory.inventoryId = hist.inventoryId
            
            WHERE TRUE " . $sqlForFilter . " 
            ORDER BY
            (case when hist.statusId=1 then 0 
            when hist.statusId=3 then 0 
            when hist.statusId=2 then 1 
			when hist.statusId=4 then 2
            else 3 end) ASC;";

$st = $conn->prepare($sql);
$st->execute();

$list = array();
while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
    $list[] = $row;
}
$conn = null;

if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'get requestList good', $list);
} else {
    echo returnStatus(0, 'get requestList fail');
}
?>
