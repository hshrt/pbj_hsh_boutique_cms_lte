<?php

require("../../config.php");
require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();
include("../checkSession.php");

if (!isset($_POST['id'])) {
    echo returnStatus(0, 'remove inventory fail ');
} else {

    $id = $_POST['id'];
    $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
    $conn->exec("set names utf8");

    $sql = "UPDATE boutique_inventory SET isVoid=1, available=0 where id = '".$id."';";

    $st = $conn->prepare($sql);
    $st->bindValue(":id", $id, PDO::PARAM_STR);
    $st->execute();

    if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
        echo returnStatus(1, 'remove inventory success!');
    } else {
        echo returnStatus(0,  $sql);
    }
    $conn = null;
}
