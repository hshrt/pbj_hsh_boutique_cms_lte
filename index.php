<?php

require("config.php"); ?>

<?php session_start();
include "php/html/header.php" ?>


    <body class="skin-blue layout-top-nav">
    <div class="callout" id="tipsBox">
        <h4 id="tipsTitle">I am a success callout!</h4>
        <p id="content">This is a green callout.</p>
    </div>
<div id="container">

    <!--if user is logged in-->
    <?php if (isset($_SESSION['email'])) { ?>
        <div id="topBar">
            <img src="images/logo.png" style="margin:20px">


            <div id="logoutBtn">Log out</div>
            <div id="seperator"> |</div>
            <div id="mailContainer">
                <div id="userEmail"></div>
            </div>


        </div>


        <div id='pages-container'>
            <div id='subhead_container'>
            </div>
            <div id='content_container' class="content-wrapper">
            </div>
        </div>

        <div id="loadContainer"></div>
    <?php } ?>

    <script>
        $(function () {

            //load the config data from PHP
            App.baseFolder = "<?php echo BASE_FOLDER; ?>";
            App.hotelLocation = "<?php echo HOTEL_LOCATION; ?>";
            App.serverType = "<?php echo SERVER_TYPE; ?>";
            App.videoInjectionURL = "<?php echo VIDEO_INJECTION_URL; ?>";

            //if user is not logged in, show Login View
            <?php if(!isset($_SESSION['email'])){?>

            App.signInView = new App.SignInView();
            <?php }else {?>

            //load the user related data from the SESSION
            App.userEmail = "<?php echo($_SESSION['email'])?>";
            App.userRole = "<?php echo($_SESSION['role'])?>";
            App.userId = "<?php echo($_SESSION['userId'])?>";

            App.init();

            $.urlParam = function (name) {
                var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                if (results == null) {
                    return '';
                }
                else {
                    return decodeURI(results[1]) || 0;
                }
            }

            $.address.change(function (event) {
                // do something depending on the event.value property, e.g.
                // $('#content').load(event.value + '.xml');
                console.log(event);

                //everytime there is a action, check whether there the session timeout, if yes, go to login page
                <?php if(!isset($_SESSION['email'])){?>

                location.reload();
                return;
                <?php }?>
                console.log("<?php echo $_SESSION['email']?>");
                //initially hide the loading
                App.hideLoading();

                var param = $.address.pathNames();

                App.subTopBar.hightlightTab(param[2]);

                console.log(param);

                $("#subhead_container").empty();
                $("#content_container").empty();

                App.subHead = null;
                App.requestList = null;
                App.boutiqueList = null;
                App.latestList = null;
                App.boutiqueDetail = null;
                App.genreList = null;
                App.languageList = null;
                App.subtitleList = null;
                App.divisionList = null;
                App.configList = null;
				App.deleteList = null;
				App.pendingList = null;
                App.openingHours = null;

                if (param[param.length - 1] == 'request') {
                    App.requestList = new App.RequestList({
                        searchDate: $.urlParam('date'),
                        searchStatus: $.urlParam('status'),
                        searchRoom: $.urlParam('room')
                    });
                } else if (param[param.length - 1] == 'product') {
                    App.boutiqueList = new App.BoutiqueList();
                } else if (param[param.length - 1] == 'new' && param[param.length - 2] == 'product') {
                    App.boutiqueDetail = new App.BoutiqueDetail({type: "new"});
                } else if (param[param.length - 2] == 'edit' || param[param.length - 3] == 'product') {
                    App.boutiqueDetail = new App.BoutiqueDetail({type: "edit", id: param[param.length - 1]});
                } else if (param[param.length - 1] == 'latest') {
                    App.latestList = new App.LatestList();
                } else if (param[param.length - 1] == 'category') {
                    App.genreList = new App.GenreList();
                } else if (param[param.length - 1] == 'language') {
                    App.languageList = new App.LanguageList();
                } else if (param[param.length - 1] == 'subtitle') {
                    App.subtitleList = new App.SubtitleList();
                } else if (param[param.length - 1] == 'division') {
                    App.divisionList = new App.DivisionList();
                } else if (param[param.length - 1] == 'config') {
                    App.configList = new App.ConfigList();
                } else if (param[param.length - 1] == 'delete') {
                    App.deleteList = new App.DeleteList();
                } else if (param[param.length - 1] == 'pending') {
                    App.pendingList = new App.PendingList();
                } else if (param[param.length - 1] == 'openingHours') {
                    App.openingHours = new App.OpeningHours();
                } else if (param.length < 1){
                    $("#content_container").append("Welcome to Boutique Management System");
                } else {
                    $("#content_container").append("Under Construction");
                }
            });

            <?php } ?>
        });
    </script>


<?php if (isset($_SESSION['email'])) {
    include "php/html/footer.php";
}
?>