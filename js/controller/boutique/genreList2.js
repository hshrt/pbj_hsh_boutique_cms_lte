App.GenreList2 = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    // It's the first function called when this view it's instantiated.
    keyObjects:null,
    keyObjectsFiltered:null,
    page:0,
    itemPerPage:15,
    filtering:false,
    initialize: function(options){
        if(options && options.listTitle){
            this.title = options.listTitle;
        }
        this.render();

    },
    events: {
    },

    render: function(){

        //alert($(window).width());
        var self = this;
        $.ajax({
            url : "php/html/listView.php?type=genre",
            method : "GET",
            dataType: "html",
        }).success(function(html){

            $(self.el).append(html).
                promise()
                .done(function(){

                });

            $("#addBtn").on("click", function(){
                App.addGenrePopup = new App.AddGenrePopup(
                    {
                        root:self
                    }
                );
            });

            $.ajax({
                url : "api/boutique/getGenreList.php",
                method : "GET",
                cache: false,
                dataType: "json",
                data : {getCount: true}
            }).success(function(json){

                self.updateNavigation(json.data[0].totalNum);

                $("#search").change(function(){
                });
                $('#search').on('keyup', function() {

                    if (this.value.length > 0) {
                        self.keyObjectsFiltered = searchStringInArr($("#search").val(),self.keyObjects);
                        $.each(self.keyObjects, function( index, obj){
                            obj.filtered = false;
                        });

                        if(self.keyObjectsFiltered == 0){
                            $("#itemContainer").empty();
                            $("#paginationContainer").hide();
                            $("#noResultMsg").show();

                        }
                        else{
                            console.log("yeah");
                            $("#paginationContainer").show();
                            $("#noResultMsg").hide();

                            self.filtering = true;
                            $.each(self.keyObjects, function( index, obj){

                                if(self.keyObjectsFiltered.indexOf(obj) > -1){
                                    obj.filtered = true;
                                }
                            });

                            self.page = 0;
                            self.loadItem(self.page);
                        }
                    }
                    else{
                        $("#paginationContainer").show();
                        $("#noResultMsg").hide();
                        self.filtering = false;
                        self.page = 0;
                        self.loadItem(self.page);
                    }
                });
                self.loadItem(self.page);

            }).error(function(d){
                console.log('error');
                console.log(d);
            });

        }).error(function(d){
            console.log('error');
            console.log(d);
        });

    },
    updateNavigation: function(_items){

        var self = this;

        $("#paginationContainer").pagination({
            items: _items,
            itemsOnPage: self.itemPerPage,
            cssStyle: 'light-theme',
            onPageClick:function(pageNum, event){
                self.page = pageNum -1;
                self.loadItem(pageNum-1);
            }
        });
    },
    goBackToFirstPage: function(){
        $("#paginationContainer").pagination('selectPage', 1);
    },
    refresh: function(){
        var self = this;
        self.keyObjects = null;
        self.keyObjectsFiltered = null;
        self.loadItem(0);
    },
    loadItem: function(page){

        var self = this;

        $("#itemContainer").empty();

        if(self.keyObjects == null){
            $.ajax({
                url : "api/boutique/getGenreList.php",
                method : "GET",
                cache: false,
                dataType: "json"
            }).success(function(json){

                self.keyObjects =  json.data;

                   //alert(self.keyObjects.length);
                //
                // $("#paginationContainer").pagination('updateItems', self.keyObjects.length);

                for(var x = page*self.itemPerPage; x < self.itemPerPage*(page+1) && x < self.keyObjects.length; x++){

                    var itemRoot =document.createElement('div');
                    $(itemRoot).addClass("itemRoot");

                    $("#itemContainer").append($(itemRoot));

                    $(itemRoot).append('<div id='+ 'listText_sc'+ x+'></div>');
                    $("#listText_sc"+ x).text(self.keyObjects[x].zh_cn);
                    $("#listText_sc"+ x).addClass("listText_sc");

                    $(itemRoot).append('<div id='+ 'global'+ x+' class = '+'button-globe '+ 'order ='+ x+  " title='click to edit'"+'></div>');
                    $(itemRoot).append("<div id="+'listText_en'+x+" class='engText'"+'></div>');

                    $("#listText_en"+x).text(self.keyObjects[x].en);

                    $("#global"+x).on('click',function(){

                        App.addGenrePopup = new App.AddGenrePopup(
                            {
                                root:App.genreList,
                                keyObj:App.genreList.keyObjects[$(this).attr("order")],
                                isEdit:true
                            }
                        );
                    });
                }

                //enable the show tip box function for swap button (a request by Chris)
                $( ".button-globe" ).tooltip();



            }).error(function(d){
                console.log('error');
                console.log(d);
            });
        }
        else{

            var keyObj = self.filtering?self.keyObjectsFiltered:self.keyObjects;

            var upperLimit = keyObj.length<self.itemPerPage?keyObj.length:self.itemPerPage;

            $("#paginationContainer").pagination('updateItems', keyObj.length);

            for(var x = page*self.itemPerPage ;x < upperLimit*(page+1) && x < self.keyObjects.length ; x++){

                //if(!self.filtering || self.keyObjects[x].filtered){
                    var itemRoot =document.createElement('div');
                    $(itemRoot).addClass("itemRoot");

                    $("#itemContainer").append($(itemRoot));

                    $(itemRoot).append('<div id='+ 'listText_sc'+ x+'></div>');
                    $("#listText_sc"+ x).text(keyObj[x].zh_cn);
                    $("#listText_sc"+ x).addClass("listText_sc");


                    $(itemRoot).append('<div id='+ 'global'+ x+' class = '+'button-globe '+ 'order ='+ x+ " title='click to edit'"+'></div>');
                    $(itemRoot).append("<div id="+'listText_en'+x+" class='engText'"+'></div>');

                    $("#listText_en"+x).text(keyObj[x].en);

                    $("#global"+x).on('click',function(){

                        App.addGenrePopup = new App.AddGenrePopup(
                            {
                                root:App.GenreList,
                                keyObj:keyObj[$(this).attr("order")],
                                isEdit:true
                            }
                        );
                    });
                //}
            }
        }
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW


        //this.$el.removeData().unbind();

        //Remove view from DOM
        $("#itemListContainer").remove();
        this.undelegateEvents();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});