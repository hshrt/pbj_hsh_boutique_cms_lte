App.InventoryList = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    // It's the first function called when this view it's instantiated.
    keyObjects: null,
    keyObjectsFiltered: null,
    page: 0,
    itemPerPage: 15,
    filtering: false,
    initialize: function (options) {
        if (options && options.listTitle) {
            this.title = options.listTitle;
        }
        this.render();

    },
    events: {},

    render: function () {

        //alert($(window).width());
        var self = this;
        $.ajax({
            url: "php/html/inventoryListView.php",
            method: "GET",
            dataType: "html",
        }).success(function (html) {

            $(self.el).append(html).promise()
                .done(function () {

                });

            $("#addBtn").on("click", function () {
                window.location = document.URL + "/new";
            });

            $.ajax({
                url: "api/boutique/getMovie.php",
                method: "GET",
                dataType: "json",
                data: {getCount: true, type: 'cms'}
            }).success(function (json) {

                self.updateNavigation(json.data[0].totalNum);

                $("#search").change(function () {
                });
                $('#search').on('keyup', function () {

                    if (this.value.length > 0) {
                        self.keyObjectsFiltered = searchStringInMovieTitle($("#search").val(), self.keyObjects);
                        $.each(self.keyObjects, function (index, obj) {
                            obj.filtered = false;
                        });

                        if (self.keyObjectsFiltered == 0) {
                            $("#itemContainer").empty();
                            $("#paginationContainer").hide();
                            $("#noResultMsg").show();

                        }
                        else {

                            $("#paginationContainer").show();
                            $("#noResultMsg").hide();

                            self.filtering = true;
                            $.each(self.keyObjects, function (index, obj) {

                                if (self.keyObjectsFiltered.indexOf(obj) > -1) {
                                    obj.filtered = true;
                                }
                            });

                            self.page = 0;
                            self.loadItem(self.page);
                        }
                    }
                    else {
                        $("#paginationContainer").show();
                        $("#noResultMsg").hide();
                        self.filtering = false;
                        self.page = 0;
                        self.loadItem(self.page);
                    }
                });
                self.loadItem(self.page);

            }).error(function (d) {
                console.log('error');
                console.log(d);
            });

        }).error(function (d) {
            console.log('error');
            console.log(d);
        });

    },
    updateNavigation: function (_items) {

        var self = this;

        $("#paginationContainer").pagination({
            items: _items,
            itemsOnPage: self.itemPerPage,
            cssStyle: 'light-theme',
            onPageClick: function (pageNum, event) {
                self.page = pageNum - 1;
                self.loadItem(pageNum - 1);
            }
        });
    },
    goBackToFirstPage: function () {
        $("#paginationContainer").pagination('selectPage', 1);
    },
    refresh: function () {
        var self = this;
        self.keyObjects = null;
        self.keyObjectsFiltered = null;
        self.loadItem(0);
    },
    loadItem: function (page) {

        var self = this;

        $("#itemContainer").empty();

        if (self.keyObjects == null) {
            $.ajax({
                url: "api/boutique/getMovie.php",
                method: "GET",
                dataType: "json",
                data: {type: "cms"}
            }).success(function (json) {

                self.keyObjects = json.data;
                for (var x = page * self.itemPerPage; x < self.keyObjects.length; x++) {
                    var isBorrowClicked = false;
                    var itemRoot = document.createElement('div');
                    $(itemRoot).addClass("itemRoot");
                    itemRoot.value = x;

                    $("#itemContainer").append($(itemRoot));

                    $(itemRoot).append('<div id=' + 'name' + x + '></div>');
                    $("#name" + x).text(self.keyObjects[x].movieTitle);
                    $("#name" + x).addClass("name");

                    $(itemRoot).append('<div id=' + 'year' + x + '></div>');
                    $("#year" + x).text(self.keyObjects[x].year);
                    $("#year" + x).addClass("year");

                    $(itemRoot).append('<div id=' + 'genre' + x + '></div>');
                    $("#genre" + x).text(self.keyObjects[x].genreTitle);
                    $("#genre" + x).addClass("genre");

                    $(itemRoot).append('<div id=' + 'stock' + x + '></div>');

                    if (self.keyObjects[x].available == null) {
                        $("#stock" + x).text(0);
                        $("#stock" + x).addClass("stock");
                    } else {
                        $("#stock" + x).text(self.keyObjects[x].available);
                        $("#stock" + x).addClass("stock");
                        $(itemRoot).append("<div id=borrow"+ x + " class=\"borrow\" order=" + x + " title='click to borrow'" + "> " +
                            "<button type=\"button\" class=\"btn btn-default \" >" +
                            "<span class=\"glyphicon glyphicon-share\" aria-hidden=\"true\"></span>" + "Borrow" +
                            "</button>" +
                            "</div>");


                        $("#borrow" + x).on('click', function () {
                            var index = $(this).attr("order");
                            isBorrowClicked = true;

                            App.addBorrowPopup = new App.AddBorrowPopup(
                                {
                                    keyObj:self.keyObjects[index]
                                }
                            );
                        });
                    }

                    $(itemRoot).on('click', function () {
                        if (!isBorrowClicked) {
                            window.location = document.URL + "/edit/" + self.keyObjects[this.value].titleId;
                        }
                    });

                    //}
                }

                //enable the show tip box function for swap button (a request by Chris)
                $(".button-globe").tooltip();


            }).error(function (d) {
                console.log('error');
                console.log(d);
            });
        }
        else {

            var keyObj = self.filtering ? self.keyObjectsFiltered : self.keyObjects;

            var upperLimit = keyObj.length < self.itemPerPage ? keyObj.length : self.itemPerPage;

            $("#paginationContainer").pagination('updateItems', keyObj.length);

            for (var x = page * self.itemPerPage; x < upperLimit * (page + 1) && x < self.keyObjects.length; x++) {
                var isBorrowClicked = false;
                var itemRoot = document.createElement('div');
                $(itemRoot).addClass("itemRoot");
                itemRoot.value = x;

                $("#itemContainer").append($(itemRoot));

                $(itemRoot).append('<div id=' + 'name' + x + '></div>');
                $("#name" + x).text(keyObj[x].movieTitle);
                $("#name" + x).addClass("name");

                $(itemRoot).append('<div id=' + 'year' + x + '></div>');
                $("#year" + x).text(keyObj[x].year);
                $("#year" + x).addClass("year");

                $(itemRoot).append('<div id=' + 'genre' + x + '></div>');
                $("#genre" + x).text(keyObj[x].genreTitle);
                $("#genre" + x).addClass("genre");

                $(itemRoot).append('<div id=' + 'stock' + x + '></div>');

                if (self.keyObjects[x].available == null) {
                    $("#stock" + x).text(0);
                    $("#stock" + x).addClass("stock");
                } else {
                    $("#stock" + x).text(keyObj[x].available);
                    $("#stock" + x).addClass("stock");
                    $(itemRoot).append("<div id=borrow"+ x + " class=\"borrow\" order=" + x + " title='click to borrow'" + "> " +
                        "<button type=\"button\" class=\"btn btn-default \" >" +
                        "<span class=\"glyphicon glyphicon-share\" aria-hidden=\"true\"></span>" + "Borrow" +
                        "</button>" +
                        "</div>");

                    $("#borrow" + x).on('click', function () {
                        var index = $(this).attr("order");
                        isBorrowClicked = true;

                        App.addBorrowPopup = new App.AddBorrowPopup(
                            {
                                keyObj:keyObj[index]
                            }
                        );
                    });
                }

                $(itemRoot).on('click', function () {
                    if (!isBorrowClicked) {
                        window.location = document.URL + "/edit/" + keyObj[this.value].titleId;
                    }
                });
            }
        }
    },
    close: function () {
        console.log("close fire");
    },
    destroy: function () {
        $("#itemListContainer").remove();
        this.undelegateEvents();
    },
    isHide: false
});