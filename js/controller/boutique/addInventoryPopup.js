App.AddInventoryPopup = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#container',
    self: null,
    room: "",
    keyObj: "",
    keyObjects: null,
    movieId: "",

    // It's the first function called when this view it's instantiated.
    initialize: function (options) {
        this.self = this;
        if (options && options.keyObj) {
            this.keyObj = options.keyObj;
            movieId = options.keyObj.movieId;

        }
        this.render();
    },
    events: {
        'click #closeBtn': 'destroy'
    },
    setupUI: function () {
        var _self = this;

        $.ajax({
            url: "api/boutique/getInventory.php",
            method: "GET",
            dataType: "json",
            cache: false,
            data: {movieId: self.movieId}
        }).success(function (json) {

            self.keyObjects = json.data;
            $("#stocklistcontainer").empty();

            $("#stocklistcontainer").append('<tr>' +
                '<th style=\"text-align: left;\" class="inventory_item_assetid\"><strong style=\"font-size: 14px;\">Stock Id</th>' +
                '<th style=\"text-align: left;\" class="inventory_item_status\"><strong style=\"font-size: 14px;\">Status</th>' +
                /*'<th style=\"text-align: left;\" class="inventory_item_room\"><strong style=\"font-size: 14px;\">Room</th>' +*/
                '<th style=\"text-align: left;\" class="inventory_item_delete\">&nbsp;</th>' + '</tr>');

            for (var x = 0; x < self.keyObjects.length; x++) {
                var itemRoot = document.createElement('tr');
                itemRoot.className = "inventory_item_table";
                $("#stocklistcontainer").append($(itemRoot));

                $(itemRoot).append('<td id=' + 'assetId' + x + '></td>');
                $("#assetId" + x).addClass("inventory_item_assetid");
                $("#assetId" + x).text(self.keyObjects[x].assetId);

                $(itemRoot).append('<td id=' + 'status' + x + '></td>');
                $("#status" + x).addClass("inventory_item_status");

                var statusString = 'Available';

                if (self.keyObjects[x].statusId == 0) {
                    statusString = 'Available';
                } else if (self.keyObjects[x].statusId == 1) {
                    statusString = 'Waiting for shipment';
                } else if (self.keyObjects[x].statusId == 2) {
                    statusString = 'Waiting for delivery';
                } else if (self.keyObjects[x].statusId == 3) {
                    statusString = 'Delivered';
                } else if (self.keyObjects[x].statusId == 4) {
                    statusString = 'Waiting for return';
                } else if (self.keyObjects[x].statusId == 5) {
                    statusString = 'Returned';
                } else if (self.keyObjects[x].statusId == 6) {
                    statusString = 'Cancelled';
                }
				if (self.keyObjects[x].available == 0)
					$("#status" + x).text('Not available');
				else 
					$("#status" + x).text('Available');
             /*   $(itemRoot).append('<td id=' + 'room' + x + '></td>');

                $("#room" + x).addClass("inventory_item_room");

                var roomString = '';
                if(self.keyObjects[x].requestTime != null) {
                    roomString = self.keyObjects[x].roomId + "  (" + self.keyObjects[x].requestTime + ")";
                } else {
                    roomString = self.keyObjects[x].roomId;
                }
                $("#room" + x).text(roomString); */

                $(itemRoot).append('<td id=' + 'td' + x + '>');
                $("#td" + x).addClass("inventory_item_delete");

                if (self.keyObjects[x].statusId == 0 && x == self.keyObjects.length - 1) {
                    $("#td" + x).append('<span id=' + 'delete' + x + ' order=' + x + ' >X Delete</span>');
                } else {
                    $("#td" + x).hide();
                }
                $(itemRoot).append('</td>');

                $("#delete" + x).addClass("inventory_item_delete_btn");

                $("#delete" + x).on('click', function () {
                    var index = $(this).attr("order");

                    App.yesNoPopup = new App.YesNoPopup(
                        {
                            yesFunc: function () {
                                $.ajax({
                                    url: "api/boutique/deleteInventory.php",
                                    method: "POST",
                                    dataType: "json",
                                    data: {
                                        id: self.keyObjects[index].id
                                    }
                                }).success(function (json) {
                                    console.log(json);
                                    if (json.status == 502) {
                                        alert(App.strings.sessionTimeOut);
                                        $('#addNewInventoryBtn').off('click')
                                        $('#delete').off('click')
                                        _self.setupUI();
                                        return;
                                    }
									$("#assetIdInput").val("");
                                    App.yesNoPopup.destroy();
                                    $('#addNewInventoryBtn').off('click')
                                    $('#delete').off('click')
                                    _self.setupUI();
                                }).error(function (d) {
                                    console.log('error');
                                    console.log(d);
                                });
                            },
						cancelFunc: function () {
							$("#assetIdInput").val("");
							App.boutiqueList.isButtonClicked = false;
							App.boutiqueList.isBorrowClicked = false;
						},
                            msg: "Are you sure remove this item?"
                        }
                    );
                });
                $('.popup_box_container').show(true);
            }

        }).error(function (d) {
            console.log('error');
            console.log(d);
        });


        $("#addNewInventoryBtn").on("click", function () {

            var assetId = $("#assetIdInput").val();
            if (assetId.length > 0) {
                App.yesNoPopup = new App.YesNoPopup(
                    {
                        yesFunc: function () {

                            $.ajax({
                                url: "api/boutique/addInventory.php",
                                method: "POST",
                                dataType: "json",
                                data: {
                                    assetId: assetId,
                                    movieId: self.movieId
                                }
                            }).success(function (json) {
                                console.log(json);

                                if (json.status == 502) {
                                    alert(App.strings.sessionTimeOut);
                                    $('#addNewInventoryBtn').off('click')
                                    $('#delete').off('click')
                                    _self.setupUI();
                                    return;
                                }
								$("#assetIdInput").val("");
                                App.yesNoPopup.destroy();
                                $('#addNewInventoryBtn').off('click')
                                $('#delete').off('click')
                                _self.setupUI();

                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });
                        },
						cancelFunc: function () {
							$("#assetIdInput").val("");
							App.boutiqueList.isButtonClicked = false;
							App.boutiqueList.isBorrowClicked = false;
						},
                        msg: "Are you sure to add this item?"
                    }
                );
            }

        });


    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function () {

        var self = this;

        $.ajax({
            url: "php/html/addInventoryPopup.php",
            method: "GET",
            dataType: "html",
            data: {}
        }).success(function (html) {
            $('#container').append(html).promise()
                .done(function () {
                    self.setupUI();
                    $('.popup_box_container').show(true);

                });
        }).error(function (d) {
            console.log('error');
            console.log(d);
        });
    },
    showUp: function () {
        $(this.el).show();
        this.isHide = false;
    },
    close: function () {
        console.log("close fire");
    },
    destroy: function () {
        $(".popup_box_container").remove();
        App.boutiqueList.refresh();
        this.undelegateEvents();
    },
    isHide: false
});