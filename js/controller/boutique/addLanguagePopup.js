App.AddLanguagePopup = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#container',
    self:null,
    keyObj:"",
    root:"",
    loading:false,
    isEdit:false,

    // It's the first function called when this view it's instantiated.
    initialize: function(options){
        this.self = this;
        if(options && options.isEdit){
            this.isEdit = options.isEdit;
        }
        if(options && options.root){
            this.root = options.root;
        }
        if(options && options.keyObj){
            this.keyObj = options.keyObj;
            console.log("this keyObj = " + this.keyObj);
            console.log("keyObj = " + this.keyObj.key);
        }

        //lock the body scroll
        $('body').css({'overflow':'hidden'});
        $(document).bind('scroll',function () {

        });

        this.render();
    },
    events: {

    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){

        var self = this;
        console.log("render AddLanguagePopup");
        $.ajax({
         url : "php/html/addLanguagePopup.php",
         method : "POST",
         dataType: "html",
         data : {}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
         }).success(function(html){
             console.log(html);
            $('#container').append(html).
                promise()
                .done(function(){
                    $('.popup_box_container').show(true);

                    $("#closeBtn").on('click',function(){
                        self.destroyItem();
                    });
                    $("#saveCreateBtn").on('click',function(){
                        if(self.isEdit)
                            self.updateItem(self.keyObj.titleId);
                        else
                            self.add();
                    });
                    $("#deleteBtn").on('click',function(){
                        self.deleteItem(self.keyObj.id);
                    });


                    if(self.isEdit){
                        //$("#chooseExistBtn").hide();
                        self.setupAllField();
                        $("#Key").attr("readonly","readonly");

                        $.ajax({
                            url: "api/boutique/getCount.php",
                            method: "GET",
                            dataType: "json",
                            data: {type: "language",
                                id: self.keyObj.id
                            }
                        }).success(function (json) {
                            if(json.data[0].totalNum > 0){
                                $("#deleteBtn").hide();
                            }
                        }).error(function (d) {
                            console.log('error');
                            console.log(d);
                        });

                    }
                    else{
                        //no need delete btn for creating new item
                        $("#deleteBtn").hide();
                    }

                });

         }).error(function(d){
            console.log('error');
            console.log(d);
         });


    },
    deleteItem:function(_id){
        App.yesNoPopup = new App.YesNoPopup(
            {
                yesFunc:function()
                {
                    $.ajax({
                        url : "api/boutique/deleteLanguage.php",
                        method : "POST",
                        dataType: "text",
                        data : {id:_id}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
                    }).success(function(json){

                        console.log(json);
                        if(json.status == 502){
                            alert(App.strings.sessionTimeOut);
                            location.reload();
                            return;
                        }

                        //self.yesFunc();
                        App.yesNoPopup.destroy();
                        App.addLanguagePopup.destroyItem();
                        App.languageList.refresh();
                        //window.history.back();

                    }).error(function(d){
                        console.log('error');
                        console.log(d);
                    });
                },
                msg:"Are you sure to delete this item?"
            }
        );
    },
    updateItem:function(_id){
        $.ajax({
            url : "api/boutique/updateDict.php",
            method : "POST",
            dataType: "json",
            data : {title_id:_id,
                    title_en: $('#English').val(),
                    title_zh_hk: $('#TraditionalChinese').val(),
                    title_zh_cn: $('#SimplifiedChinese').val(),
                    title_jp: $('#Japanese').val(),
                    title_fr: $('#French').val(),
                    title_ar: $('#Arabic').val(),
                    title_es: $('#Spanish').val(),
                    title_de: $('#German').val(),
                    title_ko: $('#Korean').val(),
                    title_ru: $('#Russian').val(),
                    title_pt: $('#Portuguese').val(),
                    title_tr: $('#Turkish').val()
            }
        }).success(function(json){
            console.log(json);
            console.log("msg = " + json.msg);

            if(json.status == 502){
                alert(App.strings.sessionTimeOut);
                location.reload();
                return;
            }

            App.addLanguagePopup.destroyItem();
            App.languageList.refresh();

        }).error(function(d){
            console.log('error');
            console.log(d);
        });
    },
    add : function(){
        if($("#SimplifiedChinese").val() == "" || $("#SimplifiedChinese").val().trim() == ""){
            alert("Simplified Chinese cannot be empty");
            return;
        }

        $.ajax({
            url : "api/boutique/addLanguage.php",
            method : "POST",
            dataType: "json",
            data : {
                title_en: $('#English').val(),
                title_zh_hk: $('#TraditionalChinese').val(),
                title_zh_cn: $('#SimplifiedChinese').val(),
                title_jp: $('#Japanese').val(),
                title_fr: $('#French').val(),
                title_ar: $('#Arabic').val(),
                title_es: $('#Spanish').val(),
                title_de: $('#German').val(),
                title_ko: $('#Korean').val(),
                title_ru: $('#Russian').val(),
                title_pt: $('#Portuguese').val(),
                title_tr: $('#Turkish').val()
            }
        }).success(function(json){
            console.log(json);
            console.log("msg = " + json.msg);

            App.addLanguagePopup.destroyItem();
            App.languageList.refresh();

        }).error(function(d){
            console.log('error');
            console.log(d);
        });
    },
    setupAllField: function(){

        console.log("keyObj = " + this.keyObj);
        console.log("keyObj = " + this.keyObj.id);
        $("#English").val(this.keyObj.en);
        $("#SimplifiedChinese").val(this.keyObj.zh_cn);
        $("#French").val(this.keyObj.fr);
        $("#Japanese").val(this.keyObj.jp);
        $("#Arabic").val(this.keyObj.ar);
        $("#Spanish").val(this.keyObj.es);
        $("#German").val(this.keyObj.de);
        $("#Korean").val(this.keyObj.ko);
        $("#Russian").val(this.keyObj.ru);
        $("#Portuguese").val(this.keyObj.pt);
        $("#TraditionalChinese").val(this.keyObj.zh_hk);
        $("#Turkish").val(this.keyObj.tr);
    },

    close :function(){
        console.log("close fire");
    },
    getSelf: function(){
        return this.self;
    },
    destroyItem: function() {
        //COMPLETELY UNBIND THE VIEW
        //this.undelegateEvents();
        //this.$el.removeData().unbind();
        $(".popup_box_container").remove();
        this.undelegateEvents();

        $(document).unbind('scroll');
        $('body').css({'overflow':'visible'});

    },
    isHide : false
});