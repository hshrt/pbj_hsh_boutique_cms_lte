App.AddLatestPopup = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#container',
    self: null,
    movieId: "",
    keyObj: "",
    keyObjects: null,
    from: null,
    requestId: "",

    // It's the first function called when this view it's instantiated.
    initialize: function (options) {
        if (options && options.keyObj) {
            this.keyObj = options.keyObj;
        }
        if (options && options.from) {
            this.from = options.from;
        }
        this.render();
    },
    events: {
        'click #closeBtn': 'destroy'
    },
    setupUIHandler: function () {

    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function () {

        var self = this;

        $.ajax({
            url: "php/html/addLatestPopup.php",
            method: "GET",
            dataType: "html",
            data: {}
        }).success(function (html) {
            $('#container').append(html).promise()
                .done(function () {

                    $.ajax({
                        url: "api/boutique/getMovieList.php",
                        method: "GET",
                        dataType: "json",
                        cache: false,
                    }).success(function (json) {

                        self.keyObjects = json.data;

                        var selectRoot = document.createElement('select');
                        selectRoot.setAttribute("id", "movieSelectionBox");
                        $("#movieSelection").append($(selectRoot));

                        for (var x = 0; x < self.keyObjects.length; x++) {
                            var movieId = self.keyObjects[x].movieId;
                            var movieTitle = self.keyObjects[x].movieTitle;
                            $(selectRoot).append("<option value='" + movieId + "'>" + movieTitle + "</option>");
                        }

                    }).error(function (d) {
                        console.log('error');
                        console.log(d);
                    });

                    $('.popup_box_container').show(true);


                    $("#confirmBtn").on('click', function () {

                        var movieId = $("#movieSelectionBox").val();

                        App.yesNoPopup = new App.YesNoPopup(
                            {
                                yesFunc: function () {

                                    $.ajax({
                                        url: "api/boutique/addLatest.php",
                                        method: "POST",
                                        dataType: "json",
                                        data: {
                                            movieId:  movieId,
                                        }
                                    }).success(function (json) {
                                        console.log(json);

                                        if (json.status == 502) {
                                            alert(App.strings.sessionTimeOut);
                                            location.reload();
                                            return;
                                        }
                                        App.yesNoPopup.destroy();
                                        App.addLatestPopup.destroy();
                                        location.reload();
                                    }).error(function (d) {
                                        console.log('error');
                                        console.log(d);
                                    });

                                },
                                msg: "Are you sure to add this product?"
                            }
                        );
                    });

                    $("#cancelBtn").on('click', function () {
                        App.addLatestPopup.destroy();
                    });

                });

        }).error(function (d) {
            console.log('error');
            console.log(d);
        });
    },

    clickConfirm: function () {
    },

    showUp: function () {
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close: function () {
        console.log("close fire");
    },
    destroy: function () {
        $(".popup_box_container").remove();
        this.undelegateEvents();
    },
    isHide: false
});