App.OpeningHours = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    // It's the first function called when this view it's instantiated.
    keyObjects:null,
    keyObjectsFiltered:null,
    page:0,
    itemPerPage:15,
    filtering:false,
    initialize: function(options){
        if(options && options.listTitle){
            this.title = options.listTitle;
        }
        this.render();

    },
    events: {
    },

    render: function(){

        //alert($(window).width());
        var self = this;
        $.ajax({
            url : "php/html/openingHours.php",
            method : "GET",
            dataType: "html",
        }).success(function(html){
            self.updateNavigation(1);
            self.loadItem(self.page);
            $(self.el).append(html).
                promise()
                .done(function(){
                });
			$( "#statusSunday").change(function() {
				if ($( "#statusSunday").val() == 'close') {
                     $('#startHourSunday').val('00');
                     $('#startHourSunday').prop('disabled', true);
					 $('#startHourSunday').css({'background-color': 'grey'});
					 $('#startMinSunday').val('00');
                     $('#startMinSunday').prop("disabled", true);
					 $('#startMinSunday').css({'background-color': 'grey'});
                     $('#endHourSunday').val('00');
                     $('#endHourSunday').prop("disabled", true);
					 $('#endHourSunday').css({'background-color': 'grey'});
                     $('#endMinSunday').val('00');
                     $('#endMinSunday').prop("disabled", true);
					 $('#endMinSunday').css({'background-color': 'grey'});
				} else {
                     $('#startHourSunday').val('00');
                     $('#startHourSunday').prop("disabled", false);
					 $('#startHourSunday').css({'background-color': 'white'});
					 $('#startMinSunday').val('00');
                     $('#startMinSunday').prop("disabled", false);
					 $('#startMinSunday').css({'background-color': 'white'});
                     $('#endHourSunday').val('00');
                     $('#endHourSunday').prop("disabled", false);
					 $('#endHourSunday').css({'background-color': 'white'});
                     $('#endMinSunday').val('00');
                     $('#endMinSunday').prop("disabled", false);
					 $('#endMinSunday').css({'background-color': 'white'});
				}
			});
			$( "#statusMonday").change(function() {
				if ($( "#statusMonday").val() == 'close') {
                     $('#startHourMonday').val('00');
                     $('#startHourMonday').prop('disabled', true);
					 $('#startHourMonday').css({'background-color': 'grey'});
					 $('#startMinMonday').val('00');
                     $('#startMinMonday').prop("disabled", true);
					 $('#startMinMonday').css({'background-color': 'grey'});
                     $('#endHourMonday').val('00');
                     $('#endHourMonday').prop("disabled", true);
					 $('#endHourMonday').css({'background-color': 'grey'});
                     $('#endMinMonday').val('00');
                     $('#endMinMonday').prop("disabled", true);
					 $('#endMinMonday').css({'background-color': 'grey'});
				} else {
                     $('#startHourMonday').val('00');
                     $('#startHourMonday').prop("disabled", false);
					 $('#startHourMonday').css({'background-color': 'white'});
					 $('#startMinMonday').val('00');
                     $('#startMinMonday').prop("disabled", false);
					 $('#startMinMonday').css({'background-color': 'white'});
                     $('#endHourMonday').val('00');
                     $('#endHourMonday').prop("disabled", false);
					 $('#endHourMonday').css({'background-color': 'white'});
                     $('#endMinMonday').val('00');
                     $('#endMinMonday').prop("disabled", false);
					 $('#endMinMonday').css({'background-color': 'white'});
				}
			});
			$( "#statusTuesday").change(function() {
				if ($( "#statusTuesday").val() == 'close') {
                     $('#startHourTuesday').val('00');
                     $('#startHourTuesday').prop('disabled', true);
					 $('#startHourTuesday').css({'background-color': 'grey'});
					 $('#startMinTuesday').val('00');
                     $('#startMinTuesday').prop("disabled", true);
					 $('#startMinTuesday').css({'background-color': 'grey'});
                     $('#endHourTuesday').val('00');
                     $('#endHourTuesday').prop("disabled", true);
					 $('#endHourTuesday').css({'background-color': 'grey'});
                     $('#endMinTuesday').val('00');
                     $('#endMinTuesday').prop("disabled", true);
					 $('#endMinTuesday').css({'background-color': 'grey'});
				} else {
                     $('#startHourTuesday').val('00');
                     $('#startHourTuesday').prop("disabled", false);
					 $('#startHourTuesday').css({'background-color': 'white'});
					 $('#startMinTuesday').val('00');
                     $('#startMinTuesday').prop("disabled", false);
					 $('#startMinTuesday').css({'background-color': 'white'});
                     $('#endHourTuesday').val('00');
                     $('#endHourTuesday').prop("disabled", false);
					 $('#endHourTuesday').css({'background-color': 'white'});
                     $('#endMinTuesday').val('00');
                     $('#endMinTuesday').prop("disabled", false);
					 $('#endMinTuesday').css({'background-color': 'white'});
				}
			});
			$( "#statusWednesday").change(function() {
				if ($( "#statusWednesday").val() == 'close') {
                     $('#startHourWednesday').val('00');
                     $('#startHourWednesday').prop('disabled', true);
					 $('#startHourWednesday').css({'background-color': 'grey'});
					 $('#startMinWednesday').val('00');
                     $('#startMinWednesday').prop("disabled", true);
					 $('#startMinWednesday').css({'background-color': 'grey'});
                     $('#endHourWednesday').val('00');
                     $('#endHourWednesday').prop("disabled", true);
					 $('#endHourWednesday').css({'background-color': 'grey'});
                     $('#endMinWednesday').val('00');
                     $('#endMinWednesday').prop("disabled", true);
					 $('#endMinWednesday').css({'background-color': 'grey'});
				} else {
                     $('#startHourWednesday').val('00');
                     $('#startHourWednesday').prop("disabled", false);
					 $('#startHourWednesday').css({'background-color': 'white'});
					 $('#startMinWednesday').val('00');
                     $('#startMinWednesday').prop("disabled", false);
					 $('#startMinWednesday').css({'background-color': 'white'});
                     $('#endHourWednesday').val('00');
                     $('#endHourWednesday').prop("disabled", false);
					 $('#endHourWednesday').css({'background-color': 'white'});
                     $('#endMinWednesday').val('00');
                     $('#endMinWednesday').prop("disabled", false);
					 $('#endMinWednesday').css({'background-color': 'white'});
				}
			});
			$( "#statusThursday").change(function() {
				if ($( "#statusThursday").val() == 'close') {
                     $('#startHourThursday').val('00');
                     $('#startHourThursday').prop('disabled', true);
					 $('#startHourThursday').css({'background-color': 'grey'});
					 $('#startMinThursday').val('00');
                     $('#startMinThursday').prop("disabled", true);
					 $('#startMinThursday').css({'background-color': 'grey'});
                     $('#endHourThursday').val('00');
                     $('#endHourThursday').prop("disabled", true);
					 $('#endHourThursday').css({'background-color': 'grey'});
                     $('#endMinThursday').val('00');
                     $('#endMinThursday').prop("disabled", true);
					 $('#endMinThursday').css({'background-color': 'grey'});
				} else {
                     $('#startHourThursday').val('00');
                     $('#startHourThursday').prop("disabled", false);
					 $('#startHourThursday').css({'background-color': 'white'});
					 $('#startMinThursday').val('00');
                     $('#startMinThursday').prop("disabled", false);
					 $('#startMinThursday').css({'background-color': 'white'});
                     $('#endHourThursday').val('00');
                     $('#endHourThursday').prop("disabled", false);
					 $('#endHourThursday').css({'background-color': 'white'});
                     $('#endMinThursday').val('00');
                     $('#endMinThursday').prop("disabled", false);
					 $('#endMinThursday').css({'background-color': 'white'});
				}
			});
			$( "#statusFriday").change(function() {
				if ($( "#statusFriday").val() == 'close') {
                     $('#startHourFriday').val('00');
                     $('#startHourFriday').prop('disabled', true);
					 $('#startHourFriday').css({'background-color': 'grey'});
					 $('#startMinFriday').val('00');
                     $('#startMinFriday').prop("disabled", true);
					 $('#startMinFriday').css({'background-color': 'grey'});
                     $('#endHourFriday').val('00');
                     $('#endHourFriday').prop("disabled", true);
					 $('#endHourFriday').css({'background-color': 'grey'});
                     $('#endMinFriday').val('00');
                     $('#endMinFriday').prop("disabled", true);
					 $('#endMinFriday').css({'background-color': 'grey'});
				} else {
                     $('#startHourFriday').val('00');
                     $('#startHourFriday').prop("disabled", false);
					 $('#startHourFriday').css({'background-color': 'white'});
					 $('#startMinFriday').val('00');
                     $('#startMinFriday').prop("disabled", false);
					 $('#startMinFriday').css({'background-color': 'white'});
                     $('#endHourFriday').val('00');
                     $('#endHourFriday').prop("disabled", false);
					 $('#endHourFriday').css({'background-color': 'white'});
                     $('#endMinFriday').val('00');
                     $('#endMinFriday').prop("disabled", false);
					 $('#endMinFriday').css({'background-color': 'white'});
				}
			});
			$( "#statusSaturday").change(function() {
				if ($( "#statusSaturday").val() == 'close') {
                     $('#startHourSaturday').val('00');
                     $('#startHourSaturday').prop('disabled', true);
					 $('#startHourSaturday').css({'background-color': 'grey'});
					 $('#startMinSaturday').val('00');
                     $('#startMinSaturday').prop("disabled", true);
					 $('#startMinSaturday').css({'background-color': 'grey'});
                     $('#endHourSaturday').val('00');
                     $('#endHourSaturday').prop("disabled", true);
					 $('#endHourSaturday').css({'background-color': 'grey'});
                     $('#endMinSaturday').val('00');
                     $('#endMinSaturday').prop("disabled", true);
					 $('#endMinSaturday').css({'background-color': 'grey'});
				} else {
                     $('#startHourSaturday').val('00');
                     $('#startHourSaturday').prop("disabled", false);
					 $('#startHourSaturday').css({'background-color': 'white'});
					 $('#startMinSaturday').val('00');
                     $('#startMinSaturday').prop("disabled", false);
					 $('#startMinSaturday').css({'background-color': 'white'});
                     $('#endHourSaturday').val('00');
                     $('#endHourSaturday').prop("disabled", false);
					 $('#endHourSaturday').css({'background-color': 'white'});
                     $('#endMinSaturday').val('00');
                     $('#endMinSaturday').prop("disabled", false);
					 $('#endMinSaturday').css({'background-color': 'white'});
				}
			});
        $("#submitBtn").on('click',function(){
            $.ajax({
                 url: "api/boutique/updateOpeningHours.php",
                 method: "POST",
                 dataType: "json",
                 data: {
                    statusSunday:$('#statusSunday').val(),
					startTimeSunday:$('#startHourSunday').val()+":"+$('#startMinSunday').val(),
					endTimeSunday:$('#endHourSunday').val()+":"+$('#endMinSunday').val(),
                    statusMonday:$('#statusMonday').val(),
					startTimeMonday:$('#startHourMonday').val()+":"+$('#startMinMonday').val(),
					endTimeMonday:$('#endHourMonday').val()+":"+$('#endMinMonday').val(),
                    statusTuesday:$('#statusTuesday').val(),
					startTimeTuesday:$('#startHourTuesday').val()+":"+$('#startMinTuesday').val(),
					endTimeTuesday:$('#endHourTuesday').val()+":"+$('#endMinTuesday').val(),
                    statusWednesday:$('#statusWednesday').val(),
					startTimeWednesday:$('#startHourWednesday').val()+":"+$('#startMinWednesday').val(),
					endTimeWednesday:$('#endHourWednesday').val()+":"+$('#endMinWednesday').val(),
                    statusThursday:$('#statusThursday').val(),
					startTimeThursday:$('#startHourThursday').val()+":"+$('#startMinThursday').val(),
					endTimeThursday:$('#endHourThursday').val()+":"+$('#endMinThursday').val(),
                    statusFriday:$('#statusFriday').val(),
					startTimeFriday:$('#startHourFriday').val()+":"+$('#startMinFriday').val(),
					endTimeFriday:$('#endHourFriday').val()+":"+$('#endMinFriday').val(),
                    statusSaturday:$('#statusSaturday').val(),
					startTimeSaturday:$('#startHourSaturday').val()+":"+$('#startMinSaturday').val(),
					endTimeSaturday:$('#endHourSaturday').val()+":"+$('#endMinSaturday').val(),
                  }
            }).success(function (json) {
                console.log(json);
                if (json.status == 502) {
                     alert(App.strings.sessionTimeOut);
                     _self.setupUI();
                     return;
                }
                alert("Update Succeed");
                 _self.setupUI();
            }).error(function (d) {
                console.log('error');
                console.log(d);
            });
        });			

        }).error(function(d){
            console.log('error');
            console.log(d);
        });
    },
    updateNavigation: function(_items){

        var self = this;

        $("#paginationContainer").pagination({
            items: _items,
            itemsOnPage: self.itemPerPage,
            cssStyle: 'light-theme',
            onPageClick:function(pageNum, event){
                self.page = pageNum -1;
                self.loadItem(pageNum-1);
            }
        });
    },
    goBackToFirstPage: function(){
        $("#paginationContainer").pagination('selectPage', 1);
    },
    refresh: function(){
        var self = this;
        self.keyObjects = null;
        self.keyObjectsFiltered = null;
        self.loadItem(0);
    },
    loadItem: function(page){

        var self = this;
        $.ajax({
            url : "api/boutique/getOpeningHours.php",
            method : "GET",
            cache: false,
            dataType: "json"
        }).success(function(json){
            self.keyObjects =  json.data;
            for(var x = 0; x < self.keyObjects.length; x++) {
				var day = self.keyObjects[x].day;
				var status = self.keyObjects[x].status;
				var startTime = self.keyObjects[x].startTime;
				var endTime = self.keyObjects[x].endTime;

				if (day == 'Sunday') {
					$('#statusSunday').val(status);
					$('#startHourSunday').val(startTime.substring(0,2));
					$('#startMinSunday').val(startTime.substring(3,5));						
					$('#endHourSunday').val(endTime.substring(0,2));	
					$('#endMinSunday').val(endTime.substring(3,5));	
					if (status == 'close') {
						$('#startHourSunday').prop('disabled', true);
						$('#startHourSunday').css({'background-color': 'grey'});
						$('#startMinSunday').prop('disabled', true);
						$('#startMinSunday').css({'background-color': 'grey'});	
						$('#endHourSunday').prop('disabled', true);
						$('#endHourSunday').css({'background-color': 'grey'});
						$('#endMinSunday').prop('disabled', true);
						$('#endMinSunday').css({'background-color': 'grey'});						
					}
				}
				if (day == 'Monday') {
					$('#statusMonday').val(status);
					$('#startHourMonday').val(startTime.substring(0,2));
					$('#startMinMonday').val(startTime.substring(3,5));						
					$('#endHourMonday').val(endTime.substring(0,2));	
					$('#endMinMonday').val(endTime.substring(3,5));	
					if (status == 'close') {
						$('#startHourMonday').prop('disabled', true);
						$('#startHourMonday').css({'background-color': 'grey'});
						$('#startMinMonday').prop('disabled', true);
						$('#startMinMonday').css({'background-color': 'grey'});	
						$('#endHourMonday').prop('disabled', true);
						$('#endHourMonday').css({'background-color': 'grey'});
						$('#endMinMonday').prop('disabled', true);
						$('#endMinMonday').css({'background-color': 'grey'});						
					}
				}
				if (day == 'Tuesday') {
					$('#statusTuesday').val(status);
					$('#startHourTuesday').val(startTime.substring(0,2));
					$('#startMinTuesday').val(startTime.substring(3,5));						
					$('#endHourTuesday').val(endTime.substring(0,2));	
					$('#endMinTuesday').val(endTime.substring(3,5));	
					if (status == 'close') {
						$('#startHourTuesday').prop('disabled', true);
						$('#startHourTuesday').css({'background-color': 'grey'});
						$('#startMinTuesday').prop('disabled', true);
						$('#startMinTuesday').css({'background-color': 'grey'});	
						$('#endHourTuesday').prop('disabled', true);
						$('#endHourTuesday').css({'background-color': 'grey'});
						$('#endMinTuesday').prop('disabled', true);
						$('#endMinTuesday').css({'background-color': 'grey'});						
					}
				}
				if (day == 'Wednesday') {
					$('#statusWednesday').val(status);
					$('#startHourWednesday').val(startTime.substring(0,2));
					$('#startMinWednesday').val(startTime.substring(3,5));						
					$('#endHourWednesday').val(endTime.substring(0,2));	
					$('#endMinWednesday').val(endTime.substring(3,5));	
					if (status == 'close') {
						$('#startHourWednesday').prop('disabled', true);
						$('#startHourWednesday').css({'background-color': 'grey'});
						$('#startMinWednesday').prop('disabled', true);
						$('#startMinWednesday').css({'background-color': 'grey'});	
						$('#endHourWednesday').prop('disabled', true);
						$('#endHourWednesday').css({'background-color': 'grey'});
						$('#endMinWednesday').prop('disabled', true);
						$('#endMinWednesday').css({'background-color': 'grey'});						
					}
				}
				if (day == 'Thursday') {
					$('#statusThursday').val(status);
					$('#startHourThursday').val(startTime.substring(0,2));
					$('#startMinThursday').val(startTime.substring(3,5));						
					$('#endHourThursday').val(endTime.substring(0,2));	
					$('#endMinThursday').val(endTime.substring(3,5));	
					if (status == 'close') {
						$('#startHourThursday').prop('disabled', true);
						$('#startHourThursday').css({'background-color': 'grey'});
						$('#startMinThursday').prop('disabled', true);
						$('#startMinThursday').css({'background-color': 'grey'});	
						$('#endHourThursday').prop('disabled', true);
						$('#endHourThursday').css({'background-color': 'grey'});
						$('#endMinThursday').prop('disabled', true);
						$('#endMinThursday').css({'background-color': 'grey'});						
					}
				}
				if (day == 'Friday') {
					$('#statusFriday').val(status);
					$('#startHourFriday').val(startTime.substring(0,2));
					$('#startMinFriday').val(startTime.substring(3,5));						
					$('#endHourFriday').val(endTime.substring(0,2));	
					$('#endMinFriday').val(endTime.substring(3,5));	
					if (status == 'close') {
						$('#startHourFriday').prop('disabled', true);
						$('#startHourFriday').css({'background-color': 'grey'});
						$('#startMinFriday').prop('disabled', true);
						$('#startMinFriday').css({'background-color': 'grey'});	
						$('#endHourFriday').prop('disabled', true);
						$('#endHourFriday').css({'background-color': 'grey'});
						$('#endMinFriday').prop('disabled', true);
						$('#endMinFriday').css({'background-color': 'grey'});						
					}
				}
				if (day == 'Saturday') {
					$('#statusSaturday').val(status);
					$('#startHourSaturday').val(startTime.substring(0,2));
					$('#startMinSaturday').val(startTime.substring(3,5));						
					$('#endHourSaturday').val(endTime.substring(0,2));	
					$('#endMinSaturday').val(endTime.substring(3,5));	
					if (status == 'close') {
						$('#startHourSaturday').prop('disabled', true);
						$('#startHourSaturday').css({'background-color': 'grey'});
						$('#startMinSaturday').prop('disabled', true);
						$('#startMinSaturday').css({'background-color': 'grey'});	
						$('#endHourSaturday').prop('disabled', true);
						$('#endHourSaturday').css({'background-color': 'grey'});
						$('#endMinSaturday').prop('disabled', true);
						$('#endMinSaturday').css({'background-color': 'grey'});
					}
				}				
			}
        }).error(function(d){
                console.log('error');
                console.log(d);
        });
   },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW


        //this.$el.removeData().unbind();

        //Remove view from DOM
        $("#itemListContainer").remove();
        this.undelegateEvents();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});