App.AddBorrowPopup = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#container',
    self: null,
    room: "",
    movieId: "",
    keyObj: "",
    keyObjects: null,
    from: null,
    requestId: "",

    // It's the first function called when this view it's instantiated.
    initialize: function (options) {
        if (options && options.keyObj) {
            this.keyObj = options.keyObj;
        }
        if (options && options.from) {
            this.from = options.from;
        }


        if (this.from == "requestlist") {
            this.movieId = options.keyObj.productId;
            this.requestId = options.keyObj.id;
        } else if (this.from == "movielist") {
            this.movieId = options.keyObj.movieId;
        }
        this.render();
    },
    events: {
        'click #closeBtn': 'destroy'
    },
    setupUIHandler: function () {

    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function () {

        var self = this;

        $.ajax({
            url: "php/html/addBorrowPopup.php",
            method: "GET",
            dataType: "html",
            data: {}
        }).success(function (html) {
            $('#container').append(html).promise()
                .done(function () {

                    $.ajax({
                        url: "api/boutique/getInventory.php",
                        method: "GET",
                        dataType: "json",
                        cache: false,
                        data: {
                            movieId: self.movieId,
                            onlyavailable: true
                        }
                    }).success(function (json) {

                        self.keyObjects = json.data;
						

                        var selectRoot = document.createElement('select');
                        selectRoot.setAttribute("id", "assetSelectionBox");
                        $("#borrowMovieAssetId").append($(selectRoot));

                        var inventoryId = self.keyObjects[0].id;
                        var assetId = self.keyObjects[0].assetId;
                        $(selectRoot).append("<option value='" + inventoryId + "'>" + assetId + "</option>");

                        if (self.from == "requestlist") {
                            $("#tr_room").hide();
                        } else if (self.from == "movielist") {
                            $('#tr_assetId').hide();
                        }

                    }).error(function (d) {
                        console.log('error');
                        console.log(d);
                    });

                    $('.popup_box_container').show(true);

                    if (self.from == "requestlist") {
                        $('#borrowMovieNameString').append(self.keyObj.title);
                    } else if (self.from == "movielist") {
                        $('#borrowMovieNameString').append(self.keyObj.movieTitle);
                    }

                    $("#confirmBtn").on('click', function () {

                        var inventoryId = "";
                        var statusId = 1;
						var customMsg = "Please confirm this item is to be shipped out";
                        if (self.from == "requestlist") {
                            self.room = self.keyObj.room;
                            inventoryId = $("#assetSelectionBox").val();
                            statusId = 2;
                        } else if (self.from == "movielist") {
							customMsg = "Are you sure to request this item?";
                            self.room = borrowMovieRoomInput.value;
                        }

                        if (self.room == null || self.room.length < 1) {
                            alert("Please input room number.");
                            return;
                        }

                        App.yesNoPopup = new App.YesNoPopup(
                            {
                                yesFunc: function () {

                                    if (self.keyObjects.length < 1) {
                                        alert("Out of Stock");
                                        App.yesNoPopup.destroy();
                                        App.addBorrowPopup.destroy();
                                    } else {
                                        var d = new Date();
                                        var currentTime = d.getFullYear() + "-" + ("00" + (d.getMonth())).slice(-2) + "-" + ("00" + (d.getDate())).slice(-2) + " " + ("00" + d.getHours()).slice(-2) + ":" + ("00" + d.getMinutes()).slice(-2)+ ":" + ("00" + d.getSeconds()).slice(-2);
                                        $.ajax({
                                            url: "api/boutique/addOrderRecord.php",
                                            method: "POST",
                                            dataType: "json",
                                            data: {
                                                movieId: self.movieId,
                                                roomId: self.room,
                                            //    inventoryId: inventoryId,
                                                statusId: 1,
												quantity: 1,
												deliveryTime: currentTime,
                                                requestId: self.requestId
                                            }
                                        }).success(function (json) {
                                            console.log(json);

                                            if (json.status == 502) {
                                                alert(App.strings.sessionTimeOut);
                                                location.reload();
                                                return;
                                            }
                                            App.yesNoPopup.destroy();
                                            App.addBorrowPopup.destroy();
                                            location.reload();
                                        }).error(function (d) {
                                            console.log('error');
                                            console.log(d);
                                        });

                                    }
                                },
                                msg: customMsg
                            }
                        );
                    });

                    $("#cancelBtn").on('click', function () {
						if (self.from != "requestlist") {
							App.boutiqueList.isButtonClicked = false;
							App.boutiqueList.isBorrowClicked = false;
						}
                        App.addBorrowPopup.destroy();
                    });

                });

        }).error(function (d) {
            console.log('error');
            console.log(d);
        });
    },

    clickConfirm: function () {
    },

    showUp: function () {
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close: function () {
        console.log("close fire");
    },
    destroy: function () {
        $(".popup_box_container").remove();
        if (self.from == "requestlist") {
            App.requestList.refresh();
        } else if (self.from == "movielist") {
            App.boutiqueList.refresh();
        }

        this.undelegateEvents();
    },
    isHide: false
});