App.SubTopBar = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#container',
    tab: "",
    highlightColor: "#fff",
    dimColor: "#453E33",
    // It's the first function called when this view it's instantiated.
    initialize: function (options) {
        this.self = this;
        if (options && options.isEdit) {
            this.isEdit = options.isEdit;
        }

        this.render();
    },
    events: {},
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function () {
        console.log("signInView render");
        var self = this;
        console.log("render SignInView");
        $.ajax({
            url: "php/html/subTopBar.php",
            method: "POST",
            dataType: "html",
            data: {}
        }).success(function (html) {
            console.log(html);
            $('#container').append(html).promise()
                .done(function () {

                    self.hightlightTab(self.tab);
					$("#latestLogo").remove();
					$("#latestTab").remove();
                    //only administrator can do admin work
                    if (App.userRole != "admin") {
                        $("#adminTab, #adminLogo").remove();
                    }
					if (App.userEmail != "operation" && App.userRole == "editor" && App.userEmail != "rolfbuehlmann@peninsula.com") {
						$("#requestLogo").remove();
						$("#requestTab").remove();
						$("#latestLogo").remove();
						$("#latestTab").remove();
						$("#genreLogo").remove();
						$("#genreTab").remove();
						$("#divisionLogo").remove();
						$("#divisionTab").remove();
						$("#configLogo").remove();
						$("#configTab").remove();
						$("#deleteLogo").remove();
						$("#deleteTab").remove();
						$("#configLogo").remove();
						$("#configTab").remove();
						$("#pendingLogo").remove();
						$("#pendingTab").remove();
						$("#openingHoursLogo").remove();
						$("#openingHoursTab").remove();						
					}
					if (App.userRole == "operation") {
						$("#latestLogo").remove();
						$("#latestTab").remove();
						$("#genreLogo").remove();
						$("#genreTab").remove();
						$("#divisionLogo").remove();
						$("#divisionTab").remove();
						$("#configLogo").remove();
						$("#configTab").remove();
						$("#deleteLogo").remove();
						$("#deleteTab").remove();
						$("#pendingLogo").remove();
						$("#pendingTab").remove();
						$("#openingHoursLogo").remove();
						$("#openingHoursTab").remove();						
					}
					if (App.userEmail == "rolfbuehlmann@peninsula.com") {
						$("#requestLogo").remove();
						$("#requestTab").remove();						
						$("#latestLogo").remove();
						$("#latestTab").remove();
						$("#genreLogo").remove();
						$("#genreTab").remove();
						$("#divisionLogo").remove();
						$("#divisionTab").remove();
						$("#configLogo").remove();
						$("#configTab").remove();
						$("#deleteLogo").remove();
						$("#deleteTab").remove();
						$("#configLogo").remove();
						$("#configTab").remove();
						$("#openingHoursLogo").remove();
						$("#openingHoursTab").remove();						
					}
                    $("#requestTab").attr("href", "#/property/" + App.hotelLocation + "/request");
                    $("#inventoryTab").attr("href", "#/property/" + App.hotelLocation + "/product");
                    $("#latestTab").attr("href", "#/property/" + App.hotelLocation + "/latest");
                    $("#genreTab").attr("href", "#/property/" + App.hotelLocation + "/category");
                    $("#languageTab").attr("href", "#/property/" + App.hotelLocation + "/language");
                    $("#subtitleTab").attr("href", "#/property/" + App.hotelLocation + "/subtitle");
                    $("#divisionTab").attr("href", "#/property/" + App.hotelLocation + "/division");
                    $("#configTab").attr("href", "#/property/" + App.hotelLocation + "/config");
					$("#deleteTab").attr("href", "#/property/" + App.hotelLocation + "/delete");
					$("#pendingTab").attr("href", "#/property/" + App.hotelLocation + "/pending");
                    $("#openingHoursTab").attr("href", "#/property/" + App.hotelLocation + "/openingHours");
                });

        }).error(function (d) {
            console.log('error');
            console.log(d);
        });


    },

    hightlightTab: function (tab) {
        this.tab = tab;
        this.dimAllTab();

        if (tab == 'request') {
            $("#requestTab").css({color: this.highlightColor});
            $("#requestLogo").css({color: this.highlightColor});
        } else if (tab == 'movie') {
            $("#inventoryTab").css({color: this.highlightColor});
            $("#inventoryLogo").css({color: this.highlightColor});
        } else if (tab == 'latest') {
            $("#latestTab").css({color: this.highlightColor});
            $("#latestLogo").css({color: this.highlightColor});
        }
        else if (tab == 'genre') {
            $("#genreTab").css({color: this.highlightColor});
            $("#genreLogo").css({color: this.highlightColor});
        }
        else if (tab == 'language') {
            $("#languageTab").css({color: this.highlightColor});
            $("#languageLogo").css({color: this.highlightColor});
        }
        else if (tab == 'subtitle') {
            $("#subtitleTab").css({color: this.highlightColor});
            $("#subtitleLogo").css({color: this.highlightColor});
        }
        else if (tab == 'division') {
            $("#divisionTab").css({color: this.highlightColor});
            $("#divisionLogo").css({color: this.highlightColor});
        }
        else if (tab == 'config') {
            $("#configTab").css({color: this.highlightColor});
            $("#configLogo").css({color: this.highlightColor});
        }
        else if (tab == 'delete') {
            $("#deleteTab").css({color: this.highlightColor});
            $("#deleteLogo").css({color: this.highlightColor});
        }
        else if (tab == 'pending') {
            $("#pendingTab").css({color: this.highlightColor});
            $("#pendingLogo").css({color: this.highlightColor});
        } 
        else if (tab == 'openingHours') {
            $("#openingHoursTab").css({color: this.highlightColor});
            $("#openingHoursLogo").css({color: this.highlightColor});
        }
    },

    dimAllTab: function () {
        $("#requestTab").css({color: this.dimColor});
        $("#inventoryTab").css({color: this.dimColor});
        $("#latestTab").css({color: this.dimColor});
        $("#genreTab").css({color: this.dimColor});
        $("#languageTab").css({color: this.dimColor});
        $("#subtitleTab").css({color: this.dimColor});
        $("#divisionTab").css({color: this.dimColor});
        $("#configTab").css({color: this.dimColor});
		$("#deleteTab").css({color: this.dimColor});
		$("#pendingTab").css({color: this.dimColor});
        $("#openingHoursTab").css({color: this.dimColor});		

        $("#requestLogo").css({color: this.dimColor});
        $("#inventoryLogo").css({color: this.dimColor});
        $("#latestLogo").css({color: this.dimColor});
        $("#genreLogo").css({color: this.dimColor});
        $("#languageLogo").css({color: this.dimColor});
        $("#subtitleLogo").css({color: this.dimColor});
        $("#divisionLogo").css({color: this.dimColor});
        $("#configLogo").css({color: this.dimColor});
		$("#deleteLogo").css({color: this.dimColor});
		$("#pendingLogo").css({color: this.dimColor});
        $("#openingHoursLogo").css({color: this.dimColor});
    },

    close: function () {
        console.log("close fire");
    },
    getSelf: function () {
        return this.self;
    },
    destroy: function () {
        //COMPLETELY UNBIND THE VIEW
        //this.undelegateEvents();
        //this.$el.removeData().unbind();
        App.imageData = null;
        App.imageFileName = null;
        $(".popup_box_container").remove();
        this.undelegateEvents();

        $(document).unbind('scroll');
        $('body').css({'overflow': 'visible'});

    },
    isHide: false
});